#ifndef MND_HELPER_HPP
#define MND_HELPER_HPP

#include <linalg/dense.hpp>

using namespace linalg;

template <typename T, typename backend>
class mnd_helper;


template <typename T>
class mnd_helper<T, blas_backend>
{
public:
    static void compute(T t, const matrix<T, blas_backend>& eqk, const matrix<T, blas_backend>& tPkq, matrix<T, blas_backend>& tempr, matrix<T, blas_backend>& tempi)
    {
        tempr.fill([t, &eqk, &tPkq] (size_t i, size_t j){return  cos(eqk(i, j)*t)*tPkq(i,j);});
        tempi.fill([t, &eqk, &tPkq] (size_t i, size_t j){return -sin(eqk(i, j)*t)*tPkq(i,j);});
    }
};


#ifdef __NVCC__
template <typename T>
class mnd_helper<T, cuda_backend>
{
public:
    static void compute(T t, const matrix<T, cuda_backend>& eqk, const matrix<T, cuda_backend>& tPkq, matrix<T, cuda_backend>& tempr, matrix<T, cuda_backend>& tempi)
    {
        tempr.fill([] __host__ __device__ (size_t i, size_t j, const T* _eqk, const T* _tPkq, size_t N, T t){return  cos(_eqk[i*N+j]*t)*_tPkq[i*N+j];}, eqk.buffer(), tPkq.buffer(), tempr.shape(1), t);
        tempi.fill([] __host__ __device__ (size_t i, size_t j, const T* _eqk, const T* _tPkq, size_t N, T t){return -sin(_eqk[i*N+j]*t)*_tPkq[i*N+j];}, eqk.buffer(), tPkq.buffer(), tempi.shape(1), t);
    }
};
#endif


#endif  //MND_HELPER_HPP


