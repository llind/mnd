#ifndef MND_HPP
#define MND_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <chrono>
#include <vector>
#include <utility>
#include <algorithm>

#include "kgrid.hpp"
#include "mnd_helper.hpp"
#include "keldesh_hamiltonian.hpp"
#include <linalg/dense.hpp>
#include <linalg/sparse.hpp>
#include <linalg/decompositions/eigensolvers/eigensolver.hpp>
#include <linalg/special_functions/determinant.hpp>

using namespace linalg;
using namespace std::chrono;

template <typename T, typename backend = blas_backend>
class mnd
{
protected:
    matrix<T, backend> Ukq;         //the eigenstates of the scattering Hamiltonian
    vector<T, backend> eq;          //the eigenvalues of the diagonal Hamiltonian
    vector<T, backend> ek;          //the eigenvalues of the scattering Hamiltonian

    kgrid<T, backend>* m_kgrid;     //the grid type used for the calculation

    T m;                            //electron mass (eV^-1 A^-2)
    T xi;                           //characteristic exciton length (A)
    T r0;                           //the screening length (A)
    size_t N;                       //number of grid points per dimension
    
    //derived parameters
    bool m_requires_ham;            //flag whether or not the Hamiltonian needs to be diagonalised and eigenvalues computed 

public:
    mnd() = delete;
    mnd(const T& _m, const T& _xi, const T& _r0, size_t _N) 
        :   Ukq(_N*_N, _N*_N), eq(_N*_N), ek(_N*_N), m_kgrid(nullptr),
            m(_m), xi(_xi), r0(_r0), N(_N), m_requires_ham(true)
    {
        ASSERT(m > 0,  "Invalid argument to mnd.  The electron mass must be greater than 0.");
    }

    ~mnd(){if(m_kgrid != nullptr){delete m_kgrid;}}
            
protected:
    //functions necessary for computing the green's function
    void construct_eigenstates()
    {
        ASSERT(m_kgrid != nullptr, "Failed to construct eigenstates the k-grid has not been bound.");
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        if(!m_kgrid->lattice_constructed())
        {
            CALL_AND_HANDLE(m_kgrid->construct(), "Failed to construct eigenstates.  Failed to construct k-grid.");
        }

        matrix<T, backend> H(N*N, N*N);
    	eigensolver<decltype(hermitian_view(H))> solver(N*N, false);

        ek = (hadamard(m_kgrid->kx(), m_kgrid->kx()) + hadamard(m_kgrid->ky(), m_kgrid->ky()))/static_cast<T>(2.0*m);
        keldesh_hamiltonian<T, backend>::construct(H, m_kgrid->kx(), m_kgrid->ky(), m_kgrid->V(), m, xi, r0);

        CALL_AND_RETHROW(solver(H, eq, Ukq, false));
        m_requires_ham = false;
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
        std::cerr << "eigenstates constructed in " << time_span.count() << " seconds" << std::endl;
    }

    //functions necessary for computing the green's function
    void construct_ek()
    {
        ASSERT(m_kgrid != nullptr, "Failed to construct eigenstates the k-grid has not been bound.");
        if(!m_kgrid->lattice_constructed())
        {
            CALL_AND_HANDLE(m_kgrid->construct(), "Failed to construct eigenstates.  Failed to construct k-grid.");
        }
        ek = (hadamard(m_kgrid->kx(), m_kgrid->kx()) + hadamard(m_kgrid->ky(), m_kgrid->ky()))/static_cast<T>(2.0*m);
    }

protected:
    size_t determine_state_contribution(T ef, T kt, vector<T, blas_backend>& h_nk, T cutoff)
    {
        vector<T, backend> ekF(N*N);
        ekF = ef;

        size_t count = 0;
        if(kt != 0.0)   
        {
            ekF = (ek - ekF)/kt;
            h_nk = ekF;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                T temp = h_nk[i];
                temp = temp > 30 ? 30 : temp;
                h_nk[i] = 1.0/(exp(temp)+1.0);
                if(h_nk[i] > cutoff){++count;}
            }
        }
        else
        {
            ekF = (ek - ekF);
            h_nk = ekF;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] <= 0.0)
                {
                    h_nk[i] = 1.0;
                    ++count;
                }
                else
                {
                    h_nk[i] = 0.0;
                }
            }
        }   
        return count;
    }
public:
    //this only works for square grids
    void trion_wavefunction(matrix<complex<T>>& psi)
    {
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        vector<T, blas_backend> h_eq(eq);
        int64_t ind = -1;
        for(size_t i=0; i<h_eq.size(); ++i)
        {
            if(h_eq[i] < 0.0)
            {
                ind = i;
                break;
            }
        }
        ASSERT(ind != -1, "Failed to find trion wavefunction.");

        matrix<T, backend> Ukqt = trans(Ukq);
        vector<T, blas_backend> psi_trion(Ukqt[ind]);
        T L = sqrt(m_kgrid->V());
        T dx = L/static_cast<T>(psi.shape(0));
        T dy = L/static_cast<T>(psi.shape(1));

        matrix<complex<T>, blas_backend> psi_x(N, psi.shape(0));
        for(size_t i=0; i<N; ++i)
        {
            T kx = (2*M_PI*i)/L;
            for(size_t _xi = 0; _xi < psi.shape(0); ++_xi)
            {
                T x = _xi*dx - L/2.0;
                psi_x(i, _xi) = complex<T>(cos(kx*x), sin(kx*x))/sqrt(L);
            }
        }

        matrix<complex<T>, blas_backend> psi_y(N, psi.shape(1));
        for(size_t i=0; i<N; ++i)
        {
            T ky = (2*M_PI*i)/L;
            for(size_t yi = 0; yi < psi.shape(1); ++yi)
            {
                T y = yi*dy - L/2.0;
                psi_y(i, yi) = complex<T>(cos(ky*y), sin(ky*y))/sqrt(L);
            }
        }

        for(size_t i=0; i<psi.shape(0); ++i)
        {
            for(size_t j=0; j<psi.shape(1); ++j)
            {
                psi(i, j) = complex<T>(0, 0);
                for(size_t nx=0; nx < N; ++nx)
                {
                    for(size_t ny=0; ny<N; ++ny)
                    {
                        psi(i, j) += psi_trion(nx*N+ny)*psi_y(ny, j)*psi_x(nx, i);
                    }
                }
            }
        }
    }


public:
    T doping_density(T ef, T kt, T cutoff=1e-5)
    {
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_ek(), "Failed to compute mnd doping density.  Failed to construct H_0.");
        }

        vector<T, blas_backend> h_nk(N*N);
        determine_state_contribution(ef, kt, h_nk, cutoff);
        T density = 0.0;
        for(size_t i=0; i<h_nk.size(); ++i)
        {
            density += h_nk[i];
        }
        return density/(m_kgrid->V());
    }

    size_t density_of_states(vector<size_t, blas_backend>& dk, vector<size_t, blas_backend>& dq, T wmin, T wmax)
    {
        ASSERT(dk.size() == dq.size(), "Failed to compute the density of states the two vectors are not the same size");
        ASSERT(dk.size() != 0, "Failed to compute the density of states the input vectors are zero size vectors.");
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        dk.fill_zeros();
        dq.fill_zeros();
        if(wmin > wmax)
        {
            T wt = wmin;
            wmin = wmax;
            wmax = wt;
        }
        T dw = (wmax - wmin)/dk.size();
        wmin = wmin - dw/2.0;
        wmax = wmax + dw/2.0;

        vector<T, blas_backend> h_ek(ek);
        vector<T, blas_backend> h_eq(eq);
        for(size_t i=0; i<N*N; ++i)
        {
            T eki = h_ek[i];
            if(eki <= wmax && eki > wmin){++dk[static_cast<size_t>((eki-wmin)/dw)];}
            T eqi = h_eq[i];
            if(eqi <= wmax && eqi > wmin){++dq[static_cast<size_t>((eqi-wmin)/dw)];}
        }
        return N*N;
    }

    size_t density_of_states(vector<T, blas_backend>& dk, vector<T, blas_backend>& dq, T wmin, T wmax, T gamma)
    {
        ASSERT(dk.size() == dq.size(), "Failed to compute the density of states the two vectors are not the same size");
        ASSERT(dk.size() != 0, "Failed to compute the density of states the input vectors are zero size vectors.");
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        dk.fill_zeros();
        dq.fill_zeros();
        if(wmin > wmax)
        {
            T wt = wmin;
            wmin = wmax;
            wmax = wt;
        }
        T dw = (wmax - wmin)/dk.size();

        vector<T, blas_backend> h_ek(ek);
        vector<T, blas_backend> h_eq(eq);
        for(size_t i=0; i<N*N; ++i)
        {
            T eki = h_ek[i];
            T eqi = h_eq[i];
            for(size_t wi = 0; wi < dk.size(); ++wi)
            {   
                T w = dw*wi + wmin;
                T dek = eki - w;
                T deq = eqi - w;
                dk[wi] += gamma*gamma/M_PI/(dek*dek + gamma*gamma);
                dq[wi] += gamma*gamma/M_PI/(deq*deq + gamma*gamma);
            }
        }
        return N*N;
    }



public:
    //R4 = <\mu(t1+t2+t3)\mu(t1+t2)\mu(t1)\mu(0)\rho>.
    //non-rephasing ground state bleach
    //in my hand written notes this is R1
    void R4(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        //we will evolve back and forward in time
        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(N*N, count);
            matrix<complex<T>, backend> t2evolve(count, N*N);

            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    ++count;
                }
            }
            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            determinant<matrix<complex<T>, backend> > det(D, false);

            //construct the truncated vector of basis vectors (but make them a complex matrix)
            //compute the matrix representation of the interior propagator - this only needs to be done once for each t2 point
            {
                diagonal_matrix<T, backend> ekm(ek);
                diagonal_matrix<complex<T>, backend> ekprop = unit_polar(-t2*ekm);
                matrix<complex<T>, backend> temp = ekprop*Ukqc;
                matrix<complex<T>, backend> T2 = Ukqc;
                Ukqc = adjoint(T2)*temp;
            }

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());
            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            for(size_t i=0; i<R.shape(0); ++i)
            {
                std::cerr << i << std::endl;
                T t1 = i*dt1;//-tmax1;
                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-t1*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Ukqc*t1evolve;
                for(size_t j=0; j<R.shape(1); ++j)
                {
                    D = Df;
                    T t3 = j*dt3;//-tmax3;
                    eqprop = unit_polar(-t3*eqm);
                    t2evolve = nPkq*eqprop;
                    temp_res = t2evolve*t1evolve2;
                
                    ekcprop = unit_polar((t1+t2+t3)*ekcm);
                    (ekcprop*temp_res).applicative(D, complex<T>(1.0, 0.0));
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1+t2+t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R4 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }

    //R2 = <\mu(0)\mu(t1+t2)\mu(t1+t2+t3)\mu(t1)\rho>
    //rephasing stimulated emission
    void R2(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);


        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(N*N, count);
            matrix<complex<T>, backend> t2evolve(count, N*N);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    ++count;
                }
            }
            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            //here we iterate over t3 loop first as that is where the most expensive operation occurs
            for(size_t j=0; j<R.shape(1); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t2+t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Ukqc*t1evolve;

                ekprop = unit_polar(t3*ekm);
                t1evolve = ekprop*t1evolve2;
                t1evolve2 = adjoint(Ukqc)*t1evolve;
                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    eqprop = unit_polar((t1+t2)*eqm);
                    t2evolve = nPkq*eqprop;
                    temp_res = t2evolve*t1evolve2;
                
                    ekcprop = unit_polar(-t1*ekcm);
                    (temp_res*ekcprop).applicative(D, complex<T>(1.0, 0.0));
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }
    
    //R3 = <\mu(0)\mu(t1)\mu(t1+t2+t3)\mu(t1+t2)\rho>
    //rephasing ground state bleach
    void R3(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(N*N, count);
            matrix<complex<T>, backend> t2evolve(count, N*N);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    ++count;
                }
            }
            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            //here we iterate over t3 loop first as that is where the most expensive operation occurs
            for(size_t j=0; j<R.shape(1); ++j)
            {
                T t3 = j*dt3;// - tmax3;
                std::cerr << j << std::endl;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Ukqc*t1evolve;

                ekprop = unit_polar((t2+t3)*ekm);
                t1evolve = ekprop*t1evolve2;
                t1evolve2 = adjoint(Ukqc)*t1evolve;
                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    eqprop = unit_polar((t1)*eqm);
                    t2evolve = nPkq*eqprop;
                    temp_res = t2evolve*t1evolve2;
                
                    ekcprop = unit_polar(-(t1+t2)*ekcm);
                    (temp_res*ekcprop).applicative(D, complex<T>(1.0, 0.0));
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R3 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }

    //R1 = <\mu(t1)\mu(t1+t2)\mu(t1+t2+t3)\mu(0)\rho>
    //non-rephasing stimulated emission 
    //in my hand written notes this is R4
    void R1(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t2evolve(count, N*N);
            matrix<complex<T>, backend> t2evolve2(count, N*N);
            matrix<complex<T>, backend> t2res(count, N*N);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    ++count;
                }
            }
            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            //precompute nPkq e^{i eq t2) adjoint(Ukq)
            {
                eqprop = unit_polar(t2*eqm);
                matrix<complex<T>, backend> temp = nPkq*eqprop;
                t2res = temp*adjoint(Ukqc);
            }

            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            //here we iterate over t3 loop first as that is where the most expensive operation occurs
            for(size_t j=0; j<R.shape(1); ++j)
            {
                T t3 = j*dt3;// - tmax3;
                std::cerr << j << std::endl;

                ekprop = unit_polar(t3*ekm);
                t2evolve = t2res*ekprop;
                t2evolve2 = t2evolve*Ukqc;
                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    eqprop = unit_polar(-(t1+t2+t3)*eqm);
                    t1evolve = eqprop*adjoint(Pkq);
                    temp_res = t2evolve2*t1evolve;
                
                    ekcprop = unit_polar(t1*ekcm);
                    (ekcprop*temp_res).applicative(D, complex<T>(1.0, 0.0));
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1+t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R1 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }

    //compute 1d current current correlation function
    void operator()(vector<complex<T>, blas_backend>& Gt, T ef, T kt, T gamma, T tmax, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }
        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt = tmax/((Gt.shape(0)-1)*hbar);

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<T, backend> nPkq(count, N*N);            
            matrix<T, backend> tPkq(N*N, count);
            matrix<T, backend> tempr(N*N, count);
            matrix<T, backend> tempi(N*N, count);
            matrix<T, backend> eqk(N*N, count);                     

            //the result matrix.
            matrix<T, backend> Dr(count, count);
            matrix<T, backend> d_Dr(count, count);
            matrix<T, backend> Di(count, count);
            matrix<complex<T>, backend> D(count, count);

            matrix<T, blas_backend> h_Dr(count, count, 0.0);
            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);

            determinant<matrix<complex<T>, backend> > det(D, false);

	        {
               	matrix<T, backend> Pkq(count, N*N);
               	count = 0;
               	for(size_t i=0; i<h_nk.size(); ++i)
               	{
               	    if(h_nk[i] > cutoff)
               	    {
               	        Pkq[count] = Ukq[i];
               	        nPkq[count] = h_nk[i]*Ukq[i];
               	        h_ekc[count] = h_ek[i];
               	        h_Dr(count, count) = (1.0 - h_nk[i]);
               	        ++count;
               	    }
               	}
               	tPkq = trans(Pkq);
	        }
	        d_Dr = h_Dr;
            
            {
                matrix<T, blas_backend> h_eqk(N*N, count, [&h_eq, &h_ekc](size_t i, size_t j){return h_eq(i) - h_ekc(j);});                     
                eqk = h_eqk;
            }

            //#pragma omp parallel for firstprivate(tempr, tempi, Dr, Di, D, det), shared(Gt, nPkq, dt, eqk, tPkq)
            for(size_t ti=0; ti < Gt.size(); ++ti)
            {
                T t = (ti*dt);//-(dt*(Gt.size()-1)/2);
                mnd_helper<T, backend>::compute(t, eqk, tPkq, tempr, tempi);
                Dr = d_Dr;
                (nPkq*tempr).applicative(Dr, 1.0);
                Di = nPkq*tempi;
                D = make_complex(Dr, Di);
                Gt(ti) = exp(complex<T>(-gamma, 0.0)*std::abs(t))*det(D, false);
            }
            
        }
        else
        {
            for(size_t ti=0; ti < Gt.size(); ++ti)
            {
                T t = ti*dt;
                Gt(ti) = exp(complex<T>(-gamma, 0.0)*t);
            }
        }


        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }

public:
    //setters and getters for the variables required by the mnd object
    const T& electron_mass() const{ return m;}
    void set_electron_mass(const T& _m)
    {
        m = _m; 
        ASSERT(m > 0,  "Invalid argument to mnd.  The electron mass must be greater than 0.");
        m_requires_ham = true;
    }

    const T& characteristic_length() const{return xi;}
    void set_characteristic_length(const T& _xi)
    {
        xi = _xi; 
        m_requires_ham = true;
    }

    const T& screening_length() const{return r0;}
    void set_screening_length(const T& _r0) 
    {
        ASSERT(_r0 > 0, "Invalid argument to mnd.  The screening length must be positive.");
        r0 = _r0; 
        m_requires_ham = true;
    }

    void bind_square_grid(T a)
    {
        if(m_kgrid != nullptr){delete m_kgrid;}
        m_kgrid = new square_kgrid<T, backend>(N, a);
        m_requires_ham = true;
    }

    //void bind_hexagonal_grid(T a)
    //{
    //    if(m_kgrid != nullptr){delete m_kgrid;}
    //    m_kgrid = new hexagonal_kgrid<T, backend>(N, a);
    //    m_requires_ham = true;
    //}

    void unbind_grid()
    {
        if(m_kgrid != nullptr){delete m_kgrid;}
        m_kgrid = nullptr;
        m_requires_ham = true;
    }

public:    
    size_t noccupied(T ef, T kt, T cutoff = 1e-5)
    {
        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);
        return count;
    }

    //R2 = <\mu(0)\mu(t1+t2)\mu(t1+t2+t3)\mu(t1)\rho>
    //rephasing stimulated emission
    void R2_proj(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, size_t to_excite)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        ASSERT(kt == 0.0, "Failed to compute the spectrum, kt=0 required.");
        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> Pkq_excite(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(count, count);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<T, blas_backend> h_ekc_excite(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            size_t count2 = 0;
            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    if(count != to_excite)
                    {
                        Pkq_excite[count2] = Ukqc[i];
                        h_ekc_excite[count2] = h_ek[i];
                        ++count2;
                    }
                    ++count;
                }
            }

            if(count2 < count)
            {
                T first_greater = 1000000;
                size_t ind_greater = 0;
                for(size_t i=0; i<h_nk.size(); ++i)
                {
                    if(h_nk[i] <= cutoff)
                    {
                        if(h_ek[i] < first_greater)
                        {
                            first_greater = h_ek[i];
                            ind_greater = i;
                        }
                    }
                }
                Pkq_excite[count2] = Ukqc[ind_greater];
                h_ekc_excite[count2] = h_ek[ind_greater];
                ++count2;
            }

            std::cout << h_ekc_excite << std::endl;
            std::cout << h_ekc << std::endl;

            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            diagonal_matrix<T, backend> ekcm_excite(h_ekc_excite);

            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            for(size_t j=0; j<R.shape(1); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t2+t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Pkq_excite*t1evolve;

                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    ekcprop = unit_polar(-t1*ekcm);

                    D = t1evolve2*ekcprop;
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }

            for(size_t i=0; i<R.shape(0); ++i)
            {
                std::cerr << i << std::endl;
                T t1 = i*dt1;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar((t1+t2)*eqm);
                t1evolve = eqprop*adjoint(Pkq_excite);
                t1evolve2 = Pkq*t1evolve;

                for(size_t j=0; j<R.shape(1); ++j)
                {
                    D = Df;
                    T t3 = j*dt3;// - tmax1;
                    ekcprop = unit_polar(t3*ekcm_excite);

                    D = t1evolve2*ekcprop;
                    R(i, j) *= det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }


    void R2_proj(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, size_t from, size_t to)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        ASSERT(kt == 0.0, "Failed to compute the spectrum, kt=0 required.");
        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);
        size_t ncount = count;

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> Pkq_excite(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(count, count);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<T, blas_backend> h_ekc_excite(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            size_t count2 = 0;
            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    if(count != from)
                    {
                        Pkq_excite[count2] = Ukqc[i];
                        h_ekc_excite[count2] = h_ek[i];
                        ++count2;
                    }
                    ++count;
                }
            }
            if(count2 < count)
            {
                std::vector<std::pair<T, size_t> > esort(h_nk.size() - ncount);

                count = 0;
                for(size_t i=0; i<h_nk.size(); ++i)
                {
                    if(h_nk[i] <= cutoff)
                    {
                        esort[count] = std::make_pair(h_ek[i], i);
                        ++count;
                    }
                }

                std::sort(esort.begin(), esort.end(), [](const std::pair<T, size_t>& a, const std::pair<T, size_t>& b) -> bool{return std::get<0>(a) < std::get<0>(b);});
                
                Pkq_excite[count2] = Ukqc[std::get<1>(esort[to])];
                h_ekc_excite[count2] = h_ek[std::get<1>(esort[to])];
                ++count2;
            }
            count = ncount;

            std::cout << h_ekc_excite << std::endl;
            std::cout << h_ekc << std::endl;

            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            diagonal_matrix<T, backend> ekcm_excite(h_ekc_excite);

            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            for(size_t j=0; j<R.shape(1); ++j)
            {
                T t3 = j*dt3;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t2+t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Pkq_excite*t1evolve;

                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    ekcprop = unit_polar(-t1*ekcm);

                    D = t1evolve2*ekcprop;
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
            std::cerr << "first pass completed" << std::endl;

            for(size_t i=0; i<R.shape(0); ++i)
            {
                T t1 = i*dt1;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar((t1+t2)*eqm);
                t1evolve = eqprop*adjoint(Pkq_excite);
                t1evolve2 = Pkq*t1evolve;

                for(size_t j=0; j<R.shape(1); ++j)
                {
                    D = Df;
                    T t3 = j*dt3;// - tmax1;
                    ekcprop = unit_polar(t3*ekcm_excite);

                    D = t1evolve2*ekcprop;
                    R(i, j) *= det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }



    //R2 = <\mu(0)\mu(t1+t2)\mu(t1+t2+t3)\mu(t1)\rho>
    //rephasing stimulated emission
    void R2_proj_fermi(matrix<complex<T>, blas_backend>& R, T ef, T kt, T tmax1, T t2, T tmax3, size_t to_excite)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        ASSERT(kt == 0.0, "Failed to compute the spectrum, kt=0 required.");
        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> Pkq_excite(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(count, count);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<T, blas_backend> h_ekc_excite(count);
            matrix<complex<T>, blas_backend> h_D(count, count);
            matrix<complex<T>, backend> Ukqc = Ukq;

            T etarget = 0.0;
            size_t count2 = 0;
            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    if(count != to_excite)
                    {
                        Pkq_excite[count2] = Ukqc[i];
                        h_ekc_excite[count2] = h_ek[i];
                        ++count2;
                    }
                    else
                    {
                        etarget = h_ek[i] + ef;
                    }
                    ++count;
                }
            }

            if(count2 < count)
            {
                T closest = 1000000;
                size_t ind_closest = 0;
                for(size_t i=0; i<h_nk.size(); ++i)
                {
                    if(h_nk[i] <= cutoff)
                    {
                        if(std::abs(h_ek[i]-etarget) < closest)
                        {
                            closest = std::abs(h_ek[i]-etarget);
                            ind_closest = i;
                        }
                    }
                }
                Pkq_excite[count2] = Ukqc[ind_closest];
                h_ekc_excite[count2] = h_ek[ind_closest];
                ++count2;
            }

            std::cout << h_ekc_excite << std::endl;
            std::cout << h_ekc << std::endl;

            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            diagonal_matrix<T, backend> ekcm_excite(h_ekc_excite);

            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            for(size_t j=0; j<R.shape(1); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t2+t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Pkq_excite*t1evolve;

                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    ekcprop = unit_polar(-t1*ekcm);

                    D = t1evolve2*ekcprop;
                    R(i, j) = det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }

            for(size_t i=0; i<R.shape(0); ++i)
            {
                std::cerr << i << std::endl;
                T t1 = i*dt1;//-tmax3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar((t1+t2)*eqm);
                t1evolve = eqprop*adjoint(Pkq_excite);
                t1evolve2 = Pkq*t1evolve;

                for(size_t j=0; j<R.shape(1); ++j)
                {
                    D = Df;
                    T t3 = j*dt3;// - tmax1;
                    ekcprop = unit_polar(t3*ekcm_excite);

                    D = t1evolve2*ekcprop;
                    R(i, j) *= det(D, false);//*exp(complex<T>(-gamma, 0.0)*(t1-t3));
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }
};

#endif  //MND_HPP//

