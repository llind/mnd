import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from scipy import interpolate
from matplotlib import animation
from mpl_toolkits.axes_grid1 import make_axes_locatable

nzeropadfactor = 4

def compute_1d_spectrum(fname, i, gamma):

    filename = fname + str(i)
    dat2 = np.genfromtxt(filename, skip_header=1)

    hbar = 4.135667696 * 1.0e-3 / (2.0*np.pi)
    dat = np.array(dat2[:, 1], dtype=np.complex128)
    dat.imag = dat2[:,2]
    dat = dat*np.exp(-gamma*dat2[:, 0]/hbar)
    
    #w1 picks up a minus sign as we are meant to be computing the fourier transform (not inverse fourier transform) with respect to w1
    w1 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[0], np.max(dat2[:, 0])/hbar/(dat.shape[0]-1)))*2*np.pi
    wf = np.fft.ifftshift(np.fft.ifft(dat, norm='ortho'))

    #finally we transpose wf so that w1 is the x axis
    return dat2[-1, 0], w1, wf
    

def compute_nrp_spectra(base, i, gamma, dtstr = None):
    f2i = None
    f2r = None
    f3i = None
    f3r = None
    if(dtstr == None):
        f2i = base+"_R1_"+str(i)+"_imag"
        f2r = base+"_R1_"+str(i)+"_real"
        f3i = base+"_R4_"+str(i)+"_imag"
        f3r = base+"_R4_"+str(i)+"_real"
    else:
        f2i = base+"_R1_"+('%f' % (i*dtstr))+"_imag"
        f2r = base+"_R1_"+('%f' % (i*dtstr))+"_real"
        f3i = base+"_R4_"+('%f' % (i*dtstr))+"_imag"
        f3r = base+"_R4_"+('%f' % (i*dtstr))+"_real"


    t1m = 0
    t3m = 0
    with open(f2r) as fp:
        firstline = fp.readline()
        t1m = float(firstline.split(" ")[0])
        t3m = float(firstline.split(" ")[0])

    dat2r = np.genfromtxt(f2r, skip_header=1)
    dat2i = np.genfromtxt(f2i, skip_header=1)
    dat3r = np.genfromtxt(f3r, skip_header=1)
    dat3i = np.genfromtxt(f3i, skip_header=1)

    #get time in inverse eV
    t1 = np.linspace(0,  t1m*2.0*np.pi, dat2r.shape[0])
    t3 = np.linspace(0,  t3m*2.0*np.pi, dat2r.shape[0])
    T1, T3 = np.meshgrid(t1, t3)
    
    dat2 = np.array(dat2r, dtype=np.complex128)
    dat2.imag = dat2i
    dat3 = np.array(dat3r, dtype=np.complex128)
    dat3.imag = dat3i
    dat = np.zeros((dat2.shape[0]*nzeropadfactor, dat2.shape[1]*nzeropadfactor), dtype=np.complex128)
    dat[0:dat3.shape[0], 0:dat3.shape[1]] = (dat2)*np.exp(-gamma*(T1+T3))
    #dat[0:dat3.shape[0], 0:dat3.shape[1]] = (dat2+dat3)*np.exp(-gamma*(T1+T3))

    w1 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[0], nzeropadfactor*t1m/(dat.shape[0]-1)))
    w3 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[1], nzeropadfactor*t3m/(dat.shape[1]-1)))

    wf = np.fft.ifftshift(np.fft.ifft2(dat, norm='ortho'))

    #transpose wf so that w1 is the x-axis
    return w1, w3, np.transpose(wf)

def compute_r2_spectra(base, i, gamma, dtstr=None):
    f2i = None
    f2r = None
    if(dtstr == None):
        f2i = base+"_R2_"+str(i)+"_imag"
        f2r = base+"_R2_"+str(i)+"_real"
    else:
        f2i = base+"_R2_"+('%f' % (i*dtstr))+"_imag"
        f2r = base+"_R2_"+('%f' % (i*dtstr))+"_real"

    t1m = 0
    t3m = 0
    with open(f2r) as fp:
        firstline = fp.readline()
        t1m = float(firstline.split(" ")[0])
        t3m = float(firstline.split(" ")[0])

    #t1 is the first index t3 is the second index
    datr = np.genfromtxt(f2r, skip_header=1)
    dati = np.genfromtxt(f2i, skip_header=1)

    #convert from t/(2*pi*hbar) to t/hbar
    t1 = np.linspace(0,  t1m*2.0*np.pi, datr.shape[0])
    t3 = np.linspace(0,  t3m*2.0*np.pi, datr.shape[1])

    #consolidate the data into a complex array 
    dat = np.array(datr, dtype=np.complex128)
    dat.imag = dati

    if(dat.shape[0] % 2 == 1):
        dat = dat[:-1, :-1]
        t1 = t1[:-1]
        t3 = t3[:-1]
        
    T1, T3 = np.meshgrid(t1, t3)

    #and now apply the exponential damping of the 
    dat = dat*np.exp(-gamma*(T1+T3))
    
    ##now compute the frequency arrays and invert the w1 array
    ##as we want to compute the forward (not backward) fourier transform
    #w1 =-np.fft.ifftshift(np.fft.fftfreq(dat.shape[0], t1[-1]/(2*np.pi*(dat.shape[0]-1))))
    #w3 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[1], t3[-1]/(2*np.pi*(dat.shape[1]-1))))

    ##compute the 2d inverse fourier 
    #wf = np.fft.ifftshift(np.fft.ifft2(dat, norm='ortho'))

    ##now we invert the index of w1 and first index of wf to order w1 in increasing order
    #w1 = w1[::-1]
    #wf = wf[::-1, :]
    ##finally we transpose wf so that w1 is the x axis
    #return w1, w3, np.transpose(wf)


    #now compute the frequency arrays and invert the w1 array
    #as we want to compute the forward (not backward) fourier transform
    w1 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[0], t1[-1]/(2*np.pi*(dat.shape[0]-1))))
    w3 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[1], t3[-1]/(2*np.pi*(dat.shape[1]-1))))

    #compute the 2d inverse fourier 
    wf = np.fft.ifftshift(np.fft.ifft2(dat, norm='ortho'))

    #finally we transpose wf so that w1 is the x axis
    return w1, w3, np.transpose(wf)


def compute_rp_spectra(base, i, gamma, dtstr):
    f2i = None
    f2r = None
    f3i = None
    f3r = None
    if(dtstr == None):
        f2i = base+"_R2_"+str(i)+"_imag"
        f2r = base+"_R2_"+str(i)+"_real"
        f3i = base+"_R3_"+str(i)+"_imag"
        f3r = base+"_R3_"+str(i)+"_real"
    else:
        f2i = base+"_R2_"+('%f' % (i*dtstr))+"_imag"
        f2r = base+"_R2_"+('%f' % (i*dtstr))+"_real"
        f3i = base+"_R3_"+('%f' % (i*dtstr))+"_imag"
        f3r = base+"_R3_"+('%f' % (i*dtstr))+"_real"

    t1m = 0
    t3m = 0
    with open(f2r) as fp:
        firstline = fp.readline()
        t1m = float(firstline.split(" ")[0])
        t3m = float(firstline.split(" ")[0])

    dat2r = np.genfromtxt(f2r, skip_header=1)
    dat2i = np.genfromtxt(f2i, skip_header=1)
    dat3r = np.genfromtxt(f3r, skip_header=1)
    dat3i = np.genfromtxt(f3i, skip_header=1)

    #get time in inverse eV
    t1 = np.linspace(0,  t1m*2.0*np.pi, dat2r.shape[0])
    t3 = np.linspace(0,  t3m*2.0*np.pi, dat2r.shape[0])
    T1, T3 = np.meshgrid(t1, t3)
    
    dat2 = np.array(dat2r, dtype=np.complex128)
    dat2.imag = dat2i
    dat3 = np.array(dat3r, dtype=np.complex128)
    dat3.imag = dat3i
    dat = np.zeros((dat3.shape[0]*nzeropadfactor, dat3.shape[1]*nzeropadfactor), dtype=np.complex128)
    dat[0:dat3.shape[0], 0:dat3.shape[1]] = (dat2+dat3)*np.exp(-gamma*(T1+T3))
    
    #w1 picks up a minus sign as we are meant to be computing the fourier transform (not inverse fourier transform) with respect to w1
    w1 =-np.fft.ifftshift(np.fft.fftfreq(dat.shape[0], nzeropadfactor*t1m/(dat.shape[0]*nzeropadfactor-1)))/2.0
    w3 = np.fft.ifftshift(np.fft.fftfreq(dat.shape[1], nzeropadfactor*t3m/(dat.shape[1]*nzeropadfactor-1)))/2.0

    wf = np.fft.ifftshift(np.fft.ifft2(dat, norm='ortho'))

    #now we invert the index of w1 and first index of wf to order w1 in increasing order
    w1 = w1[::-1]
    wf = wf[::-1, :]

    #finally we transpose wf so that w1 is the x axis
    return w1, w3, np.transpose(wf)


def plotrp(filebase, nframes, delta, gamma, minw, maxw, dts, dt=None):
    W1, W3, Rrp  = compute_rp_spectra(filebase, 0, gamma, dt)
    datrp = np.zeros((nframes, Rrp.shape[0], Rrp.shape[1]), dtype=np.complex128)
    
    temp = Rrp#np.transpose(Rrp)
    datrp[0, :, :] = temp[:, :]
    scalefactor = nzeropadfactor*Rrp.shape[0]//500

    loaded = np.zeros((nframes), dtype = int)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    div = make_axes_locatable(ax)
    cax = div.append_axes('right', '5%', '5%')
    
    #cmap_type = 'nipy_spectral'
    cmap_type = 'Spectral_r'
        
    scalemin = 0.7
    scalemax = 1.0
    minshift = 1e-2
    hbar = 4.135668 * 1.0e-3 / (2.0*np.pi)

    #im = ax.imshow(np.real(datrp[0,:,:]), cmap=cmap_type, origin = 'lower', extent = [delta+W1[0], delta+W1[-1], delta+W3[0], delta+W3[-1]],vmin=np.min(np.real(Rrp))*scalemax, vmax = np.max(np.real(Rrp)*scalemax))
    im = ax.imshow(np.abs(datrp[0,:,:]), cmap=cmap_type, origin = 'lower', extent = [delta+W1[0], delta+W1[-1], delta+W3[0], delta+W3[-1]], vmin=minshift, vmax = np.max(np.abs(Rrp)*scalemax))
    
    tx = ax.set_title(r'$t_2 = 0$ ps')
    cbar = fig.colorbar(im, cax=cax)
   
    ax.set_xlabel(r'$\hbar\omega_1$ (meV)')
    ax.set_ylabel(r'$\hbar\omega_3$ (meV)')
    ax.set_xlim(delta+minw, delta+maxw)
    ax.set_ylim(delta+minw, delta+maxw)
    ax.plot(W1+delta, W3+delta, 'k--', scalex=False, scaley=False, linewidth=0.5)
    
    def animate_func(i):
        if(loaded[i] == 0):
            W1, W3, temp = compute_rp_spectra(filebase, i, gamma, dt)
            temp2 = temp#np.transpose(temp)
            datrp[i, :, :] = temp2[:,:]
    
        im.set_array(np.abs(datrp[i, :, :]))
        #im.set_array(np.real(datrp[i, :, :]))
    
        tx.set_text(r'$t_2 =$ %.2f ps' % (i*dts))
        cbar = fig.colorbar(im, cax=cax)
        if(loaded[i] == 0):
            loaded[i] = 1
            plt.savefig(filebase+"_rp_"+str(i)+".png")
            #plt.savefig(filebase+"_rp_"+str(i)+".png")
        return [im]
        
    anim = animation.FuncAnimation( fig, 
                                    animate_func, 
                                    frames = nframes,
                                    interval = 1000 / 30, # in ms
                                    repeat=True
                                   )
    plt.show()



def plot_coherence_peaks(filebase, nframes, delta, gamma, indx, indy, dts, dt=None):
    print(filebase)
    plt.clf()
    t = np.arange(nframes)*dts*1000
    y1 = np.zeros(nframes)
    y2 = np.zeros(nframes)

    for i in range(nframes):
        W1, W3, Rrp  = compute_rp_spectra(filebase, i, gamma, dt)
        y1[i] = np.abs(Rrp[indx, indy])
        y2[i] = np.abs(Rrp[indy, indx])

    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot(t, y1/np.maximum(y1[0], y2[0]), 'k-', label="LCP")
    ax.plot(t, y2/np.maximum(y1[0], y2[0]), 'r-', label="HCP")
    ax.set_xlabel(r'$t_2$ (fs)')
    ax.set_ylabel('Normalised Amplitude')
    #ax.set_xlim([0, 150])
    plt.legend()
    plt.savefig(filebase+"_off_diagonals.png")
    #plt.savefig(filebase+"_off_diagonals_short_time.png")
    #plt.show()


def plotr2_slices(filebase, nframes, delta, gamma, minw, maxw, df, dfs=None):
    W1, W3, Rrp  = compute_r2_spectra(filebase, 0, gamma, dfs)
    datrp = np.zeros((nframes, Rrp.shape[0], Rrp.shape[1]), dtype=np.complex128)
    print(W1.shape, W3.shape, Rrp.shape)
    for i in range(nframes):
        if not i == 0:
            W1, W3, temp = compute_r2_spectra(filebase, i, gamma, dfs)
            print(W1.shape, W3.shape, Rrp.shape)
            temp2 = temp
            datrp[i, :, :] = temp2[:,:]
            
    
    temp = Rrp#np.transpose(Rrp)
    datrp[0, :, :] = temp[:, :]
    scalefactor = Rrp.shape[0]//500

    levels = np.linspace(-1.0, 1.0, 50)
    levelsb = np.zeros(30)
    levelsb[:10] = levels[:20:2]
    levelsb[10:20] = levels[20:30]
    levelsb[20:30] = levels[30:50:2]

    cmap_type = 'bwr'
    #cmap_type = 'Spectral_r'
    if(True):
        loaded = np.zeros((nframes), dtype = int)

        fig2 = plt.figure(1, figsize=(8,8))
        ax2 = fig2.add_subplot(111, aspect='equal')
        div2 = make_axes_locatable(ax2)
        cax2 = div2.append_axes('right', '5%', '5%')
        w1, w3 = np.meshgrid(W1, W3)

        im2 = ax2.contourf(w1+delta, w3+delta, np.real(datrp[0,:,:])/np.max(np.abs(np.real(Rrp))), cmap=cmap_type, levels=levels)

        ax2.contour(w1+delta, w3+delta, np.real(datrp[0,:,:])/np.max(np.abs(np.real(Rrp))), colors='k', levels=levelsb, linewidths = 0.5)
        tx2 = ax2.set_title(r'$\varepsilon_f = 0$ meV')
        cbar2 = fig2.colorbar(im2, cax=cax2)

        
        ax2.set_xlabel(r'$\hbar\omega_1$ (eV)')
        ax2.set_ylabel(r'$\hbar\omega_3$ (eV)')
        ax2.set_xlim(delta+minw, delta+maxw)
        ax2.set_ylim(delta+minw, delta+maxw)
            
        for i in range(nframes):
            maxr = np.max(np.abs(np.real(datrp[i,:,:])))
            plt.clf()
            fig2 = plt.figure(1, figsize=(8,8))
            ax2 = fig2.add_subplot(111, aspect='equal')
            div2 = make_axes_locatable(ax2)
            cax2 = div2.append_axes('right', '5%', '5%')
            ax2.contourf(w1+delta, w3+delta, np.real(datrp[i,:,:])/maxr, cmap=cmap_type, levels=levels)
            ax2.contour(w1+delta, w3+delta, np.real(datrp[i,:,:])/maxr, colors='k', levels=levelsb, linewidths = 0.5)
            tx2 = ax2.set_title(r'$t_2 =$ %.2f ps' % (i*df))
            cbar2 = fig2.colorbar(im2, cax=cax2)

            
            ax2.set_xlabel(r'$\hbar\omega_1$ (meV)')
            ax2.set_ylabel(r'$\hbar\omega_3$ (meV)')
            ax2.set_xlim(delta+minw, delta+maxw)
            ax2.set_ylim(delta+minw, delta+maxw)
            print(i)
            if(loaded[i] == 0):
                loaded[i] = 1
                plt.savefig("nonrephasing_se_"+str(i)+".svg", bbox_inches='tight')
        plt.clf()

    if(False):
        for i in range(nframes):
            plt.clf()
            dat1d = np
            fig3=plt.figure(3, figsize=(8,8))
            dv = np.abs(datrp[i,:,:])#/np.max(np.abs(np.real(Rrp)))

            tmax, w1d, spec = compute_1d_spectrum(filebase.rsplit('/', 1)[0]+"/out_1d_", i, 0.002)
            spec = spec / (w1d+delta)
            tscale = tmax/5.0
            print(tscale)
            
            first_max = None
            second_max = None
            for ii in range(int(spec.shape[0]*0.4), int(spec.shape[0]*0.6)):
                if(spec[ii-1] < spec[ii] and spec[ii+1] < spec[ii] and spec[ii]/np.max(spec) - spec[-1]/np.max(spec) > 0.01):
                    if(first_max == None):
                        first_max = ii
                    else:   
                        second_max = ii
                        if(second_max > 0.5*spec.shape[0]):
                            break
            if(second_max == None):
                second_max = first_max
            i1 = int(first_max/tscale*0.5*nzeropadfactor)+2
            i2 = int(second_max/tscale*0.5*nzeropadfactor)+2

            #plt.plot(spec)
            #plt.show()

            #i1 = np.abs(datrp[0, :, ii1:]).max(axis=0).argmax()+ii1
            #i2 = np.abs(datrp[0, :, :ii2]).max(axis=0).argmax()

            print(i1, i2)
            max1 = np.max(np.abs(datrp[i, :, i1]))
            max2 = np.max(np.abs(datrp[i, i1, :]))
            max3 = np.max(np.abs(datrp[i, :, i2]))
            max4 = np.max(np.abs(datrp[i, i2, :]))
            plt.subplots_adjust(wspace=0, hspace=0)
            ax3 = fig3.add_subplot(311)
            tx2 = ax3.set_title(r'$\varepsilon_f =$ %.2f meV' % (i*df))
            ax3.set_ylim([0.0, 1.0])
            ax3.set_xlim([delta+minw, delta+maxw])
            ax3.plot(W3+delta, dv[:,i1]/max1, 'k')
            ax3.text(minw+delta, 0.9, r'$\hbar\omega_1 = $ %.3f eV' % (W1[i1]+delta))
            ax3.plot(np.ones(2)*(delta+W1[i1]), np.array([0, 1]), 'r')
            #ax3.set_xlabel(r'$\hbar\omega_3$ (meV)')

            ax3 = fig3.add_subplot(312)
            ax3.set_ylim([0.0, 1.0])
            ax3.set_xlim([delta+minw, delta+maxw])
            ax3.plot(W3+delta, dv[:,i2]/max3, 'k')
            ax3.set_xlabel(r'$\hbar\omega_3$ (eV)')
            ax3.text(minw+delta, 0.9, r'$\hbar\omega_1 = $ %.3f eV' % (W1[i2]+delta))
            ax3.plot(np.ones(2)*(delta+W1[i2]), np.array([0, 1]), 'r')

            tmax, w1d, spec = compute_1d_spectrum(filebase.rsplit('/', 1)[0]+"/out_1d_", i, 0.002)
            spec = spec / (w1d+delta)
            ax3 = fig3.add_subplot(313)
            ax3.set_ylim([0.0, 1.0])
            ax3.set_xlim([delta+minw, delta+maxw])
            ax3.plot(w1d[::int(tscale)]+delta, np.real(spec[::int(tscale)])/np.max(np.real(spec[int(0.4*spec.shape[0]):int(0.6*spec.shape[0])])), 'k')
            ax3.set_xlabel(r'$\hbar\omega_1$ (eV)')
        

            #ax3 = fig3.add_subplot(222)
            #ax3.set_ylim([0.0, 1.0])
            #ax3.set_xlim([delta+minw, delta+maxw])
            #ax3.plot(W3+delta, dv[i1,:]/max2, 'k')
            #ax3.text(minw+delta, 0.9, r'$\hbar\omega_3 = $ %.3f meV' % (W3[i1]+delta))

            #ax3 = fig3.add_subplot(224)
            #ax3.set_ylim([0.0, 1.0])
            #ax3.set_xlim([delta+minw, delta+maxw])
            #ax3.plot(W3+delta, dv[i2,:]/max4, 'k')
            #ax3.set_xlabel(r'$\hbar\omega_1$ (meV)')
            #ax3.text(minw+delta, 0.9, r'$\hbar\omega_3 = $ %.3f meV' % (W3[i2]+delta))
            plt.savefig(filebase.split("/")[-2]+"_"+str(i)+".svg", bbox_inches='tight')

    plt.close()

def plotr2(filebase, nframes, delta, gamma, minw, maxw, dts, x1=0, x2=0, dt=None):
    W1, W3, Rrp  = compute_r2_spectra(filebase, 0, gamma, dt)
    datrp = np.zeros((nframes, Rrp.shape[0], Rrp.shape[1]), dtype=np.complex128)
    print(W1.shape, W3.shape, Rrp.shape)
    for i in range(nframes):
        if not i == 0:
            W1, W3, temp = compute_r2_spectra(filebase, i, gamma, dt)
            print(W1.shape, W3.shape, Rrp.shape)
            temp2 = temp
            datrp[i, :, :] = temp2[:,:]
            
    
    temp = Rrp#np.transpose(Rrp)
    datrp[0, :, :] = temp[:, :]
    scalefactor = Rrp.shape[0]//500

    loaded = np.zeros((nframes), dtype = int)
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111, aspect='equal')
    div = make_axes_locatable(ax)
    cax = div.append_axes('right', '5%', '5%')
    
    cmap_type = 'bwr'
    #cmap_type = 'Spectral_r'
        
    scalemin = 0.7
    scalemax = 0.7
    minshift = 1e-2
    hbar = 4.135668 * 1.0e-3 / (2.0*np.pi)

    #im = ax.imshow(np.real(datrp[0,:,:]), cmap=cmap_type, origin = 'lower')
    #plt.show()
    

    if(True):
        w1, w3 = np.meshgrid(W1, W3)
        levels = np.linspace(0.0, 1.0, 50)
        im = ax.contourf(w1+delta, w3+delta, np.abs(datrp[0,:,:])/np.max(np.abs(np.abs(Rrp))), cmap=cmap_type, levels=levels)

        ax.contour(w1+delta, w3+delta, np.abs(datrp[0,:,:])/np.max(np.abs(np.abs(Rrp))), colors='k', levels=levels[::3], linewidths = 0.5)

        tx = ax.set_title(r'$t_2 = 0$ ps')
        cbar = fig.colorbar(im, cax=cax)
        
        ax.set_xlabel(r'$\hbar\omega_1$ (meV)')
        ax.set_ylabel(r'$\hbar\omega_3$ (meV)')
        ax.set_xlim(delta+minw, delta+maxw)
        ax.set_ylim(delta+minw, delta+maxw)
        ax.plot(W1+delta, W3+delta, 'k--', scalex=False, scaley=False, linewidth=0.5)

        
        def animate_func(i):
            im.set_array(np.abs(datrp[i, :, :]))
        
            tx.set_text(r'$t_2 =$ %.2f ps' % (i*dts))
            cbar = fig.colorbar(im, cax=cax)
            if(loaded[i] == 0):
                loaded[i] = 1
                plt.savefig(filebase+"_r2_abs_"+str(i)+".png")
            return [im]
            
        anim = animation.FuncAnimation( fig, 
                                        animate_func, 
                                        frames = nframes,
                                        interval = 1000 / 30, # in ms
                                        repeat=True
                                       )

        plt.show()
        plt.clf()

    levels = np.linspace(-1.0, 1.0, 50)
    levelsb = np.zeros(30)
    levelsb[:10] = levels[:20:2]
    levelsb[10:20] = levels[20:30]
    levelsb[20:30] = levels[30:50:2]
    if(False):
        loaded = np.zeros((nframes), dtype = int)

        fig2 = plt.figure(1, figsize=(8,8))
        ax2 = fig2.add_subplot(111, aspect='equal')
        div2 = make_axes_locatable(ax2)
        cax2 = div2.append_axes('right', '5%', '5%')
        #im2 = ax2.imshow(np.real(datrp[0,:,:])/np.max(np.abs(np.real(Rrp))), cmap=cmap_type, origin = 'lower', extent = [delta+W1[0], delta+W1[-1], delta+W3[0], delta+W3[-1]], vmin=-1.0, vmax = 1.0)
        w1, w3 = np.meshgrid(W1, W3)
        print(W1.shape)

        im2 = ax2.contourf(w1+delta, w3+delta, np.real(datrp[0,:,:])/np.max(np.abs(np.real(Rrp))), cmap=cmap_type, levels=levels)

        ax2.contour(w1+delta, w3+delta, np.real(datrp[0,:,:])/np.max(np.abs(np.real(Rrp))), colors='k', levels=levelsb, linewidths = 0.5)
        tx2 = ax2.set_title(r'$t_2 = 0$ ps')
        cbar2 = fig2.colorbar(im2, cax=cax2)

        
        ax2.set_xlabel(r'$\hbar\omega_1$ (meV)')
        ax2.set_ylabel(r'$\hbar\omega_3$ (meV)')
        ax2.set_xlim(delta+minw, delta+maxw)
        ax2.set_ylim(delta+minw, delta+maxw)

        #fig3=plt.figure(3)
        #dv = np.abs(datrp[0,:,:])#/np.max(np.abs(np.real(Rrp)))
        #plt.subplots_adjust(wspace=0, hspace=0)
        #ax3 = fig3.add_subplot(221)
        #ax3.plot(W3, dv[:,520], 'k')
        #ax3 = fig3.add_subplot(223)
        #ax3.plot(W3, dv[520,:], 'k')
        #ax3.set_xlabel(r'$\hbar\omega_3$ (meV)')
        #ax3 = fig3.add_subplot(222)
        #ax3.plot(W3, dv[:,434], 'k')
        #ax3 = fig3.add_subplot(224)
        #ax3.plot(W3, dv[434,:], 'k')
        #ax3.set_xlabel(r'$\hbar\omega_1$ (meV)')
        

        def animate_func_2(i):
            #im2.set_array(np.real(datrp[i, :, :])/np.max(np.abs(np.real(datrp[i, :, :]))))
            tx2.set_text(r'$t_2 =$ %.2f ps' % (i*dts))
            cbar2 = fig2.colorbar(im2, cax=cax2)
            if(loaded[i] == 0):
                loaded[i] = 1
                plt.savefig(filebase+"_r2_real_"+str(i)+".png")
            return [cont]
            
        maxr = np.max(np.abs(np.real(datrp[:,:,:])))
        for i in range(nframes):
            plt.clf()
            fig2 = plt.figure(1, figsize=(8,8))
            ax2 = fig2.add_subplot(111, aspect='equal')
            div2 = make_axes_locatable(ax2)
            cax2 = div2.append_axes('right', '5%', '5%')
            ax2.contourf(w1+delta, w3+delta, np.real(datrp[i,:,:])/maxr, cmap=cmap_type, levels=levels)
            ax2.contour(w1+delta, w3+delta, np.real(datrp[i,:,:])/maxr, colors='k', levels=levelsb, linewidths = 0.5)
            tx2 = ax2.set_title(r'$t_2 =$ %.2f ps' % (i*dts))
            cbar2 = fig2.colorbar(im2, cax=cax2)

            
            ax2.set_xlabel(r'$\hbar\omega_1$ (eV)')
            ax2.set_ylabel(r'$\hbar\omega_3$ (eV)')
            ax2.set_xlim(delta+minw, delta+maxw)
            ax2.set_ylim(delta+minw, delta+maxw)
            #cbar2 = fig2.colorbar(im2, cax=cax2)
            print(i)
            if(loaded[i] == 0):
                loaded[i] = 1
                plt.savefig("r2_real_"+str(i)+".svg", bbox_inches='tight')
            #plt.show()
        plt.clf()

    if(True):
        maxr = np.max(np.abs(np.real(Rrp)))
        dv = np.real(datrp[:,:,:])#/maxr
        for i in range(nframes):
            plt.clf()
            fig3=plt.figure(3, figsize=(8,8))
            dvi = dv[i, :, :]

            i1 = x1*scalefactor
            i2 = x2*scalefactor
            #max1 = np.max(np.abs(datrp[:, :, i1]))
            #max2 = np.max(np.abs(datrp[:, i1, :]))
            #max3 = np.max(np.abs(datrp[:, :, i2]))
            #max4 = np.max(np.abs(datrp[:, i2, :]))
            plt.subplots_adjust(wspace=0, hspace=0)
            ax3 = fig3.add_subplot(111)
            tx2 = ax3.set_title(r'$t_2 =$ %.2f ps' % (i*dts))
            ax3.set_ylim([-0.1, 0.3])
            #ax3.set_ylim([-np.max(dv[:,:,i1]), np.max(dv[:,:,i1])])
            ax3.set_xlim((delta+minw)*1000, (delta+maxw)*1000)
            ax3.plot((W3+delta)*1000, dvi[:,i1], 'k')
            #ax3.text(-0.2+delta, 0.9, r'$\hbar\omega_1 = $ %.3f meV' % (W3[i1]+delta))
            ax3.set_xlabel(r'$\hbar\omega_3 - \Delta$ (meV)')

            #ax3 = fig3.add_subplot(223)
            #ax3.set_ylim([0.0, 1.0])
            #ax3.plot(W3+delta, dv[:,i2]/max3, 'k')
            #ax3.set_xlabel(r'$\hbar\omega_3$ (meV)')
            #ax3.text(-0.2+delta, 0.9, r'$\hbar\omega_1 = $ %.3f meV' % (W3[i2]+delta))

            #ax3 = fig3.add_subplot(222)
            #ax3.set_ylim([0.0, 1.0])
            #ax3.plot(W3+delta, dv[i1,:]/max2, 'k')
            #ax3.text(-0.2+delta, 0.9, r'$\hbar\omega_3 = $ %.3f meV' % (W3[i1]+delta))

            #ax3 = fig3.add_subplot(224)
            #ax3.set_ylim([0.0, 1.0])
            #ax3.plot(W3+delta, dv[i2,:]/max4, 'k')
            #ax3.set_xlabel(r'$\hbar\omega_1$ (meV)')
            #ax3.text(-0.2+delta, 0.9, r'$\hbar\omega_3 = $ %.3f meV' % (W3[i2]+delta))
            plt.savefig(filebase.split("/")[-2]+"_%02d.svg" % i, bbox_inches='tight')

        #anim2 = animation.FuncAnimation(fig2, 
        #                                animate_func_2, 
        #                                frames = nframes,
        #                                interval = 1000 / 30, # in ms
        #                                repeat=True
        #                               )
        #plt.show()
    plt.close()

if __name__ == "__main__":
    delta = 0.0
    filebase1 = "out_100"
    gamma = 0.005
    minw = -0.05
    maxw =  0.05
    nframes = 5

    #plotr2_slices("fermi_scan/out", nframes, 2.0, gamma, minw, maxw, 2.0)
    #plotr2_slices("../N_120_correct/fermi_50/out_fermi_50120", nframes, 2.0, gamma, minw, maxw, 0.02)
    #plotr2_slices("../N_120_correct/fermi_10/outb", nframes, 2.0, gamma, minw, maxw, 0.02)
    plotr2("out_MoSe2_40_2d_large_0.010000", nframes, 2.0, gamma, minw, maxw, 0.02, 253, 225)
    #plotr2("../N_120_correct/fermi_10/outb", nframes, 2.0, gamma, minw, maxw, 0.02, 257, 224)
    #plotr2("../N_120_correct/fermi_20/out_fermi_20120", nframes, delta, gamma, minw, maxw, 0.02, 266, 221)
    #plotr2("../N_120_correct/fermi_30/out_120", nframes, 2.0, gamma, minw, maxw, 0.02, 277, 219)
    #plotr2("../N_120_correct/fermi_50/out_fermi_50120", nframes, 2.0, gamma, minw, maxw, 0.02, 298, 216)


    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_10/outb", 0, gamma)

    #plt.plot(np.array([0.01, 0.02, 0.03, 0.05]), np.array([W3[257]-W3[248], W3[266]-W3[243], W3[277]-W3[242], W3[299]-W3[238]]), 'ko-')
    #plt.xlim(0, 0.05)
    #plt.ylim(0, 0.07)

    #x=np.linspace(0, 0.05, 100)
    #plt.plot(x, x)
    #plt.show()
    #datrp = np.zeros((nframes, Rrp.shape[0], Rrp.shape[1]), dtype=np.complex128)
    #for i in range(nframes):
    #    W1, W3, temp = compute_r2_spectra("../N_120_correct/fermi_10/outb", i, gamma)
    #    print(W1.shape, W3.shape, Rrp.shape)
    #    temp2 = temp
    #    datrp[i, :, :] = temp2[:,:]

    #dat_t = np.real(datrp[:, 248, 257])
    #plt.plot(np.linspace(0, dat_t.shape[0]-1, dat_t.shape[0])*0.02, dat_t, 'o-', color='tab:blue', label = '10 meV')
    #plt.xlim(0, (dat_t.shape[0]-1)*0.02)
    #plt.ylim(0, 0.3)

    #for i in range(nframes):
    #    W1, W3, temp = compute_r2_spectra("../N_120_correct/fermi_20/out_fermi_20120", i, gamma)
    #    print(W1.shape, W3.shape, Rrp.shape)
    #    temp2 = temp
    #    datrp[i, :, :] = temp2[:,:]

    #dat_t = np.real(datrp[:, 243, 266])
    #plt.plot(np.linspace(0, dat_t.shape[0]-1, dat_t.shape[0])*0.02, dat_t, 'o-', color='tab:cyan', label = '20 meV')

    #for i in range(nframes):
    #    W1, W3, temp = compute_r2_spectra("../N_120_correct/fermi_30/out_120", i, gamma)
    #    print(W1.shape, W3.shape, Rrp.shape)
    #    temp2 = temp
    #    datrp[i, :, :] = temp2[:,:]

    #dat_t = np.real(datrp[:, 242, 277])
    #plt.plot(np.linspace(0, dat_t.shape[0]-1, dat_t.shape[0])*0.02, dat_t, 'o-', color='tab:orange', label = '30 meV')

    #for i in range(nframes):
    #    W1, W3, temp = compute_r2_spectra("../N_120_correct/fermi_50/out_fermi_50120", i, gamma)
    #    print(W1.shape, W3.shape, Rrp.shape)
    #    temp2 = temp
    #    datrp[i, :, :] = temp2[:,:]

    #dat_t = np.real(datrp[:, 238, 299])
    #plt.plot(np.linspace(0, dat_t.shape[0]-1, dat_t.shape[0])*0.02, dat_t, 'o-', color='tab:red', label = '50 meV')
    #plt.legend()
    #plt.xlabel(r'$t_2$ (ps)')
    #plt.savefig("peak_height_20.svg", bbox_inches='tight')

    frame = 0

    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_5/out_fermi_5120", frame, gamma, None)
    #i1 = 253
    #plt.ylim([-0.05, 0.75])
    #plt.xlim((delta+minw)*1000, (delta+maxw)*1000)
    #plt.plot((W3+delta)*1000, np.real(Rrp[:,i1]), 'k', label='5 meV')

    #i1 = 257
    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_10/outb", frame, gamma, None)
    #plt.plot((W3+delta)*1000, np.real(Rrp[:,i1]), 'tab:blue', label='10 meV')

    #i1 = 266
    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_20/out_fermi_20120", frame, gamma, None)
    #plt.plot((W3+delta)*1000, np.real(Rrp[:,i1]), 'tab:cyan', label='20 meV')

    #i1 = 277
    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_30/out_120", frame, gamma, None)
    #plt.plot((W3+delta)*1000, np.real(Rrp[:,i1]), 'tab:orange', label='30 meV')

    #i1 = 298
    #W1, W3, Rrp  = compute_r2_spectra("../N_120_correct/fermi_50/out_fermi_50120", frame, gamma, None)
    #plt.plot((W3+delta)*1000, np.real(Rrp[:,i1]), 'tab:red', label='50 meV')
    #plt.xlabel(r'$\hbar\omega_3 - \Delta$ (meV)')
    #plt.legend()
    #plt.show()
    #plt.savefig("all_fermi.svg", bbox_inches='tight')

    #nframes = 12
    #filebase2 = "fermi_50/out_fermi_50120"
    ##plotr2(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    ##plotrp(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    #plot_coherence_peaks(filebase2, nframes, delta, gamma, 598, 433, 0.02)

    #minw = -0.05
    #maxw =  0.05
    #nframes = 30
    #filebase2 = "fermi_30/out_120"
    ##plotr2(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    ##plotrp(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    #plot_coherence_peaks(filebase2, nframes, delta, gamma, 555, 438, 0.02)
    #nframes = 50
    #filebase2 = "fermi_5/out_fermi_5120"
    ##plotr2(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    ##plotrp(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    #plot_coherence_peaks(filebase2, nframes, delta, gamma, 507, 449, 0.02)
    #nframes = 50
    #filebase2 = "fermi_10/outb"
    ##plotr2(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    ##plotrp(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    #plot_coherence_peaks(filebase2, nframes, delta, gamma, 516, 447, 0.02)
    #nframes = 37
    #filebase2 = "fermi_20/out_fermi_20120"
    ##plotr2(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    ##plotrp(filebase2, nframes, delta, gamma, minw, maxw, 0.02)
    #plot_coherence_peaks(filebase2, nframes, delta, gamma, 534, 442, 0.02)
