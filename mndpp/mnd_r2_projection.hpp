#ifndef MND_R2_HPP
#define MND_R2_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <chrono>
#include <vector>
#include <utility>
#include <algorithm>

#include "kgrid.hpp"
#include "mnd_helper.hpp"
#include "keldesh_hamiltonian.hpp"
#include <linalg/dense.hpp>
#include <linalg/sparse.hpp>
#include <linalg/decompositions/eigensolvers/eigensolver.hpp>
#include <linalg/special_functions/determinant.hpp>


template <typename backend>
struct projection_mask;

template <typename backend>
struct generalised_cauchy_binet;

template <>
struct projection_mask<blas_backend>
{
    using backend = blas_backend;
    template <typename T> 
    static inline void apply(const matrix<T, backend>& m, size_t a, const vector<size_t, backend>& occ, matrix<T, backend>& r)
    {
        ASSERT(m.shape() == r.shape(), "Failed to apply mask to form r matrix.");
        ASSERT(a < m.shape(1), "Invalid indices.");
        for(size_t i=0; i<m.shape(0); ++i)
        {
            for(size_t j=0; j<m.shape(1); ++j)
            {
                r(i, j) = m(i, j);
            }
            for(size_t ind = 0; ind < occ.shape(0); ++ind)
            {
                if(ind != a)
                {
                    r(i, occ(ind)) = T(0.0);
                }
            }
        }
    }
};


template <>
struct generalised_cauchy_binet<blas_backend>
{
    using backend = blas_backend;
    template <typename T> 
    static void build(const matrix<T, backend>& M1, const matrix<T, backend>& M2, const matrix<T, backend>& M, size_t o, const vector<size_t, backend>& occ, matrix<T, backend>& r)
    {
        size_t k = occ.shape(0);
        //set the upper right corner
        for(size_t i=0; i<k-1; ++i)
        {
            for(size_t j=0; j<k-1; ++j){r(i, j) = T(0);}
            size_t m2iind = i < o ? i : i+1;
            for(size_t j=0; j<k; ++j)
            {
                size_t rjind = j+(k-1);
                r(i, rjind) = M2(occ(m2iind), j);
            }
        }

        //now we set the lower two blocks
        for(size_t i=0; i<k; ++i)
        {
            size_t riind = i + (k-1);
            for(size_t j=0; j < k-1; ++j)
            {
                size_t m1jind = j < o ? j : j+1;
                r(riind, j) = M1(i, occ(m1jind));
            }
            for(size_t j=0; j < k; ++j)
            {
                size_t rjind = j+(k-1);
                r(riind, rjind) = M(i, j);
            }
        }
    }
};

#ifdef __NVCC__
namespace proj_cuda_kernel
{
    template<typename T>
    __global__ void apply_mask(T* out, const size_t m, const size_t n, const size_t a, const size_t* occ, const size_t count)
    {
        unsigned int tidx, tidy;
        unsigned int stridex = blockDim.x * gridDim.x;
        unsigned int stridey = blockDim.y * gridDim.y;

        #pragma unroll
        for(tidx = threadIdx.x + blockDim.x * blockIdx.x; tidx < m; tidx += stridex)
        {
            for(tidy = threadIdx.y + blockDim.y * blockIdx.y; tidy < count; tidy += stridey)
            {
                if(tidy != a)
                {
                    out[tidx*n+occ[tidy]] = T(0);
                }
            }
        }
    }
}

template <>
struct projection_mask<cuda_backend>
{
    using backend = cuda_backend;
    template <typename T> 
    static inline void apply(const matrix<T, backend>& mm, size_t a, const vector<size_t, backend>& occ, matrix<T, backend>& r)
    {
        ASSERT(mm.shape() == r.shape(), "Failed to apply mask to form r matrix.");
        ASSERT(a < mm.shape(1) , "Invalid indices.");

        r = mm;

        ASSERT(cuda_backend::is_initialised(), "cuda backend operation failed.  The cuda environment has not yet been initialised.");
        dim3 dg, db;
        size_t mnt = cuda_backend::environment().maximum_threads_per_block();
        size_t mnx = cuda_backend::environment().maximum_dimensions_threads_per_block()[0];
        size_t mny = cuda_backend::environment().maximum_dimensions_threads_per_block()[1];

        size_t m = mm.shape(0);
        size_t n = occ.shape(0);
        if(n >= mnt)
        {
            db.y = mnt < mny ? mnt : mny;
            db.x = mnt/db.y;
        }
        else
        {
            if(n >= 16 && (n & (n - 1)) == 0){ db.y = n < mny ? n : mny; db.x = mnt/db.y; } //if m >= 16 and m is a power of 2 then we use m as the value for db.x
            else{db.y = 16 < mny ? 16 : mny; db.x = mnt / db.y;}    //otherwise we will just use 16 for db.x
        }
        db.x = db.x < mnx ? db.x : mnx;
        dg.x = (m+db.x-1)/db.x;
        dg.y = (n+db.y-1)/db.y;

        proj_cuda_kernel::apply_mask<<<dg, db, 0, cuda_backend::environment().current_stream()>>>(r.buffer(), m, mm.shape(1), a, occ.buffer(), occ.shape(0));
    }
};

namespace proj_cuda_kernel
{
    template<typename T>
    __global__ void build_mat(const T * M1, const T* M2, const T* M, T* out, const size_t m, const size_t n, const size_t a, const size_t* occ, const size_t k, const size_t m1n, const size_t m2n)
    {
        unsigned int tidx, tidy;
        unsigned int stridex = blockDim.x * gridDim.x;
        unsigned int stridey = blockDim.y * gridDim.y;

        #pragma unroll
        for(tidx = threadIdx.x + blockDim.x * blockIdx.x; tidx < m; tidx += stridex)
        {
            if(tidx < k-1)
            {
                for(tidy = threadIdx.y + blockDim.y * blockIdx.y; tidy < n; tidy += stridey)
                {
                    if(tidy < k-1){out[tidx*n+tidy] = T(0);}
                    else
                    {
                        unsigned int idy = tidy - (k-1);    
                        unsigned int idx = tidx < a ? tidx : tidx+1;
                        out[tidx*n+tidy] = M2[occ[idx]*m2n + idy];
                    }
                }
            }
            else
            {
                unsigned int idx = tidx - (k-1);
                for(tidy = threadIdx.y + blockDim.y * blockIdx.y; tidy < n; tidy += stridey)
                {
                    if(tidy < k-1)
                    {
                        unsigned int idy = tidy < a ? tidy : tidy+1;
                        out[tidx*n+tidy] = M1[idx*m1n + occ[idy]];
                    }
                    else
                    {
                        unsigned int idy = tidy - (k-1);    
                        out[tidx*n+tidy] = M[idx*k + idy];
                    }
                }
            }
        }
    }
}

template <>
struct generalised_cauchy_binet<cuda_backend>
{
    using backend = cuda_backend;
    template <typename T> 
    static void build(const matrix<T, backend>& M1, const matrix<T, backend>& M2, const matrix<T, backend>& M, size_t o, const vector<size_t, backend>& occ, matrix<T, backend>& r)
    {
        ASSERT(cuda_backend::is_initialised(), "cuda backend operation failed.  The cuda environment has not yet been initialised.");
        dim3 dg, db;
        size_t mnt = cuda_backend::environment().maximum_threads_per_block();
        size_t mnx = cuda_backend::environment().maximum_dimensions_threads_per_block()[0];
        size_t mny = cuda_backend::environment().maximum_dimensions_threads_per_block()[1];

        size_t k = occ.shape(0);
        size_t m = 2*k-1;
        size_t n = 2*k-1;
        if(n >= mnt)
        {
            db.y = mnt < mny ? mnt : mny;
            db.x = mnt/db.y;
        }
        else
        {
            if(n >= 16 && (n & (n - 1)) == 0){ db.y = n < mny ? n : mny; db.x = mnt/db.y; } //if m >= 16 and m is a power of 2 then we use m as the value for db.x
            else{db.y = 16 < mny ? 16 : mny; db.x = mnt / db.y;}    //otherwise we will just use 16 for db.x
        }
        db.x = db.x < mnx ? db.x : mnx;
        dg.x = (m+db.x-1)/db.x;
        dg.y = (n+db.y-1)/db.y;

        proj_cuda_kernel::build_mat<<<dg, db, 0, cuda_backend::environment().current_stream()>>>(M1.buffer(), M2.buffer(), M.buffer(), r.buffer(), m, n, o, occ.buffer(), occ.shape(0), M1.shape(1), M2.shape(1));

    }
};
#endif


template <typename T, typename backend = blas_backend>
class mnd_r2
{
protected:
    matrix<T, backend> Ukq;         //the eigenstates of the scattering Hamiltonian
    matrix<complex<T>, backend> Ukqc;         //the eigenstates of the scattering Hamiltonian
    vector<T, backend> eq;          //the eigenvalues of the diagonal Hamiltonian
    vector<T, backend> ek;          //the eigenvalues of the scattering Hamiltonian

    kgrid<T, backend>* m_kgrid;     //the grid type used for the calculation

    T m;                            //electron mass (eV^-1 A^-2)
    T xi;                           //characteristic exciton length (A)
    T r0;                           //the screening length (A)
    size_t N;                       //number of grid points per dimension
    
    //derived parameters
    bool m_requires_ham;            //flag whether or not the Hamiltonian needs to be diagonalised and eigenvalues computed 

public:
    mnd_r2() = delete;
    mnd_r2(const T& _m, const T& _xi, const T& _r0, size_t _N) 
        :   Ukq(_N*_N, _N*_N), eq(_N*_N), ek(_N*_N), m_kgrid(nullptr),
            m(_m), xi(_xi), r0(_r0), N(_N), m_requires_ham(true)
    {
        ASSERT(m > 0,  "Invalid argument to mnd_r2.  The electron mass must be greater than 0.");
    }

    ~mnd_r2(){if(m_kgrid != nullptr){delete m_kgrid;}}
            
protected:
    //functions necessary for computing the green's function
    void construct_eigenstates()
    {
        {
            Ukq.resize(N*N, N*N);
            ASSERT(m_kgrid != nullptr, "Failed to construct eigenstates the k-grid has not been bound.");\
            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            if(!m_kgrid->lattice_constructed())
            {
                CALL_AND_HANDLE(m_kgrid->construct(), "Failed to construct eigenstates.  Failed to construct k-grid.");
            }

            matrix<T, backend> H(N*N, N*N);
            eigensolver<decltype(hermitian_view(H))> solver(N*N, false);

            ek = (hadamard(m_kgrid->kx(), m_kgrid->kx()) + hadamard(m_kgrid->ky(), m_kgrid->ky()))/static_cast<T>(2.0*m);
            keldesh_hamiltonian<T, backend>::construct(H, m_kgrid->kx(), m_kgrid->ky(), m_kgrid->V(), m, xi, r0);

            CALL_AND_RETHROW(solver(H, eq, Ukq, false));
            m_requires_ham = false;
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
            std::cerr << "eigenstates constructed in " << time_span.count() << " seconds" << std::endl;
        }
        Ukqc.resize(Ukq.shape(0), Ukq.shape(1));
        CALL_AND_HANDLE(Ukqc = Ukq, "Failed to set complex basis functions.");
    }

    //functions necessary for computing the green's function
    void construct_ek()
    {
        ASSERT(m_kgrid != nullptr, "Failed to construct eigenstates the k-grid has not been bound.");
        if(!m_kgrid->lattice_constructed())
        {
            CALL_AND_HANDLE(m_kgrid->construct(), "Failed to construct eigenstates.  Failed to construct k-grid.");
        }
        ek = (hadamard(m_kgrid->kx(), m_kgrid->kx()) + hadamard(m_kgrid->ky(), m_kgrid->ky()))/static_cast<T>(2.0*m);
    }

protected:
    size_t determine_state_contribution(T ef, T kt, vector<T, blas_backend>& h_nk, T cutoff)
    {
        vector<T, backend> ekF(N*N);
        ekF = ef;

        size_t count = 0;
        if(kt != 0.0)   
        {
            ekF = (ek - ekF)/kt;
            h_nk = ekF;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                T temp = h_nk[i];
                temp = temp > 30 ? 30 : temp;
                h_nk[i] = 1.0/(exp(temp)+1.0);
                if(h_nk[i] > cutoff){++count;}
            }
        }
        else
        {
            ekF = (ek - ekF);
            h_nk = ekF;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] <= 0.0)
                {
                    h_nk[i] = 1.0;
                    ++count;
                }
                else
                {
                    h_nk[i] = 0.0;
                }
            }
        }   
        return count;
    }
public:
    //this only works for square grids
    void trion_wavefunction(matrix<complex<T>>& psi)
    {
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }

        vector<T, blas_backend> h_eq(eq);
        int64_t ind = -1;
        for(size_t i=0; i<h_eq.size(); ++i)
        {
            if(h_eq[i] < 0.0)
            {
                ind = i;
                break;
            }
        }
        ASSERT(ind != -1, "Failed to find trion wavefunction.");

        matrix<T, backend> Ukqt = trans(Ukq);
        vector<T, blas_backend> psi_trion(Ukqt[ind]);
        T L = sqrt(m_kgrid->V());
        T dx = L/static_cast<T>(psi.shape(0));
        T dy = L/static_cast<T>(psi.shape(1));

        matrix<complex<T>, blas_backend> psi_x(N, psi.shape(0));
        for(size_t i=0; i<N; ++i)
        {
            T kx = (2*M_PI*i)/L;
            for(size_t _xi = 0; _xi < psi.shape(0); ++_xi)
            {
                T x = _xi*dx - L/2.0;
                psi_x(i, _xi) = complex<T>(cos(kx*x), sin(kx*x))/sqrt(L);
            }
        }

        matrix<complex<T>, blas_backend> psi_y(N, psi.shape(1));
        for(size_t i=0; i<N; ++i)
        {
            T ky = (2*M_PI*i)/L;
            for(size_t yi = 0; yi < psi.shape(1); ++yi)
            {
                T y = yi*dy - L/2.0;
                psi_y(i, yi) = complex<T>(cos(ky*y), sin(ky*y))/sqrt(L);
            }
        }

        for(size_t i=0; i<psi.shape(0); ++i)
        {
            for(size_t j=0; j<psi.shape(1); ++j)
            {
                psi(i, j) = complex<T>(0, 0);
                for(size_t nx=0; nx < N; ++nx)
                {
                    for(size_t ny=0; ny<N; ++ny)
                    {
                        psi(i, j) += psi_trion(nx*N+ny)*psi_y(ny, j)*psi_x(nx, i);
                    }
                }
            }
        }
    }


public:
    T doping_density(T ef, T kt, T cutoff=1e-5)
    {
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_ek(), "Failed to compute mnd_r2 doping density.  Failed to construct H_0.");
        }

        vector<T, blas_backend> h_nk(N*N);
        determine_state_contribution(ef, kt, h_nk, cutoff);
        T density = 0.0;
        for(size_t i=0; i<h_nk.size(); ++i)
        {
            density += h_nk[i];
        }
        return density/(m_kgrid->V());
    }

    size_t density_of_states(vector<size_t, blas_backend>& dk, vector<size_t, blas_backend>& dq, T wmin, T wmax)
    {
        ASSERT(dk.size() == dq.size(), "Failed to compute the density of states the two vectors are not the same size");
        ASSERT(dk.size() != 0, "Failed to compute the density of states the input vectors are zero size vectors.");
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }

        dk.fill_zeros();
        dq.fill_zeros();
        if(wmin > wmax)
        {
            T wt = wmin;
            wmin = wmax;
            wmax = wt;
        }
        T dw = (wmax - wmin)/dk.size();
        wmin = wmin - dw/2.0;
        wmax = wmax + dw/2.0;

        vector<T, blas_backend> h_ek(ek);
        vector<T, blas_backend> h_eq(eq);
        for(size_t i=0; i<N*N; ++i)
        {
            T eki = h_ek[i];
            if(eki <= wmax && eki > wmin){++dk[static_cast<size_t>((eki-wmin)/dw)];}
            T eqi = h_eq[i];
            if(eqi <= wmax && eqi > wmin){++dq[static_cast<size_t>((eqi-wmin)/dw)];}
        }
        return N*N;
    }

    size_t density_of_states(vector<T, blas_backend>& dk, vector<T, blas_backend>& dq, T wmin, T wmax, T gamma)
    {
        ASSERT(dk.size() == dq.size(), "Failed to compute the density of states the two vectors are not the same size");
        ASSERT(dk.size() != 0, "Failed to compute the density of states the input vectors are zero size vectors.");
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }

        dk.fill_zeros();
        dq.fill_zeros();
        if(wmin > wmax)
        {
            T wt = wmin;
            wmin = wmax;
            wmax = wt;
        }
        T dw = (wmax - wmin)/dk.size();

        vector<T, blas_backend> h_ek(ek);
        vector<T, blas_backend> h_eq(eq);
        for(size_t i=0; i<N*N; ++i)
        {
            T eki = h_ek[i];
            T eqi = h_eq[i];
            for(size_t wi = 0; wi < dk.size(); ++wi)
            {   
                T w = dw*wi + wmin;
                T dek = eki - w;
                T deq = eqi - w;
                dk[wi] += gamma*gamma/M_PI/(dek*dek + gamma*gamma);
                dq[wi] += gamma*gamma/M_PI/(deq*deq + gamma*gamma);
            }
        }
        return N*N;
    }



public:
    //R2_S = <\mu(0)\mu_S(t1+t2)\mu_S(t1+t2+t3)\mu(t1)\rho>
    //the singly excited state resolved rephasing stimulated emission
    //This exploits a generalisation of the Cauchy-Binet formula for evaluating a subset of permutations giving rise to the full determinant
    //That allows for all single excitations involving removing an electron from a given occupied orbital to be evaluated by evaluating a single determinant.
    //If state is a number less than noccupied and greater than zero then this function will only compute contributions where an annihilation operator acts on this state
    //(although it will also include the ground state contribution).
    //If orbital is < 0 then it will compute the contribution from all of the states and it will include noccupied contributions from the ground state
    //This is currently incorrect, it does not include the contributions from the ground state term - there is something very wrong.
    size_t R2_single_excitation(matrix<complex<T>, blas_backend>& R, T ef, T tmax1, T t2, T tmax3, int64_t orbital = -1)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }
        clear_temporaries();

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, 0.0, h_nk, cutoff);
        size_t ncount = count;

        ASSERT(orbital < static_cast<int64_t>(count), "Cannot include single excitations out of an unoccupied orbital.");

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> M2temp(N*N, count);
            matrix<complex<T>, backend> M2(N*N, count);
            matrix<complex<T>, backend> M1(count, N*N);
            matrix<complex<T>, backend> M1_masked(count, N*N);

            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<size_t, blas_backend> h_occ(count);

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_occ[count] = i;
                    ++count;
                }
            }
            vector<size_t, backend> occ(h_occ);
            matrix<complex<T>, backend> D(2*count-1, 2*count-1, complex<T>(0, 0));
            matrix<complex<T>, backend> M(count, count);

            diagonal_matrix<T, backend> ekcm(h_ekc);    h_ekc.clear();
            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(count);

            h_ek.clear();

            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            //here we iterate over t3 loop first as that is where the most expensive operation occurs
            for(size_t j=0; j<R.shape(1); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;

                //now that we have specified t3 we can compute the contribution to M2 involving evolution in the exciton manifold of states
                //and the contribution to M1 corresponding to evolution on the ground state manifold
                eqprop = unit_polar(-(t2+t3)*eqm);
                M2 = eqprop*adjoint(Pkq);
                M2temp = Ukqc*M2;

                ekprop = unit_polar(t3*ekm);
                for(size_t i=0; i<R.shape(0); ++i)
                {
                    T t1 = i*dt1;
                    //first we use M2 as a temporary matrix so that we can compute M1 
                    M2.resize(count, N*N);
                    eqprop = unit_polar((t1+t2)*eqm);
                    M1 = Pkq*eqprop;
                    M2 = M1*adjoint(Ukqc);      //this is the most expensive step of the calculation and I can't reasonably remove it
                    M1 = M2*ekprop;

                    M2.resize(N*N, count);
                    //now we can act the component of M2 corresponding to evolution on the ground state manifold 
                    ekcprop = unit_polar(-t1*ekcm);
                    M2 = M2temp*ekcprop;

                    complex<T> temp_val(0, 0);
                    if(orbital < 0)     
                    {
                        for(size_t orb = 0; orb < ncount; ++orb)
                        {
                            projection_mask<backend>::apply(M1, orb, occ, M1_masked);
                            //now we can compute the M matrix 
                            
                            M = M1_masked*M2;
                            //now that we have the correctly contracted M matrix we can construct the full D matrix:w
                            generalised_cauchy_binet<backend>::build(M1, M2, M, orb, occ, D);
                            temp_val += det(D, false);
                        }
                    }
                    else
                    {
                        projection_mask<backend>::apply(M1, orbital, occ, M1_masked);
                        M = M1_masked*M2;
                        generalised_cauchy_binet<backend>::build(M1, M2, M, orbital, occ, D);
                        temp_val += det(D, false);
                    }
                    if(count % 2 == 0)
                    {
                        R(i, j) = -temp_val;
                    }
                    else
                    {
                        R(i, j) = temp_val;
                    }
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
        if(orbital < 0)
        {
            return count;
        }
        else
        {
            return 1;
        }
    }


    //R2_S = <\mu(0)\mu_S(t1+t2)\mu_S(t1+t2+t3)\mu(t1)\rho>
    //the singly excited state resolved rephasing stimulated emission
    //This exploits a generalisation of the Cauchy-Binet formula for evaluating a subset of permutations giving rise to the full determinant
    //That allows for all single excitations involving removing an electron from a given occupied orbital to be evaluated by evaluating a single determinant.
    //If state is a number less than noccupied and greater than zero then this function will only compute contributions where an annihilation operator acts on this state
    //(although it will also include the ground state contribution).
    //If orbital is < 0 then it will compute the contribution from all of the states and it will include noccupied contributions from the ground state
    //This is currently incorrect, it does not include the contributions from the ground state term - there is something very wrong.
    size_t R2_single_excitation(tensor<complex<T>, 3, blas_backend>& R, T ef, T tmax1, T t2, T tmax3, int64_t first_orbital)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }
        clear_temporaries();

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(1)-1)*hbar);
        T dt3 = tmax3/((R.shape(2)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, 0.0, h_nk, cutoff);
        size_t ncount = count;

        ASSERT(first_orbital < static_cast<int64_t>(count), "Cannot include single excitations out of an unoccupied orbital.");

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> M2temp(N*N, count);
            matrix<complex<T>, backend> M2(N*N, count);
            matrix<complex<T>, backend> M1(count, N*N);
            matrix<complex<T>, backend> M1_masked(count, N*N);

            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<size_t, blas_backend> h_occ(count);

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_occ[count] = i;
                    ++count;
                }
            }
            vector<size_t, backend> occ(h_occ);
            matrix<complex<T>, backend> D(2*count-1, 2*count-1, complex<T>(0, 0));
            matrix<complex<T>, backend> M(count, count);

            diagonal_matrix<T, backend> ekcm(h_ekc);    h_ekc.clear();
            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(count);

            h_ek.clear();

            //now we can iterate over all of the t1 and t2 time points and construct the 3 time correlation functions at the points (t1, t2, t3)
            //here we iterate over t3 loop first as that is where the most expensive operation occurs
            for(size_t j=0; j<R.shape(2); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;

                //now that we have specified t3 we can compute the contribution to M2 involving evolution in the exciton manifold of states
                //and the contribution to M1 corresponding to evolution on the ground state manifold
                eqprop = unit_polar(-(t2+t3)*eqm);
                M2 = eqprop*adjoint(Pkq);
                M2temp = Ukqc*M2;

                ekprop = unit_polar(t3*ekm);
                for(size_t i=0; i<R.shape(1); ++i)
                {
                    T t1 = i*dt1;
                    //first we use M2 as a temporary matrix so that we can compute M1 
                    M2.resize(count, N*N);
                    eqprop = unit_polar((t1+t2)*eqm);
                    M1 = Pkq*eqprop;
                    M2 = M1*adjoint(Ukqc);      //this is the most expensive step of the calculation and I can't reasonably remove it
                    M1 = M2*ekprop;

                    M2.resize(N*N, count);
                    //now we can act the component of M2 corresponding to evolution on the ground state manifold 
                    ekcprop = unit_polar(-t1*ekcm);
                    M2 = M2temp*ekcprop;

                    for(size_t orb = 0; orb < R.shape(0) && orb+first_orbital < ncount; ++orb)
                    {
                        complex<T> temp_val(0, 0);
                        projection_mask<backend>::apply(M1, orb+first_orbital, occ, M1_masked);
                        //now we can compute the M matrix 
                        
                        M = M1_masked*M2;
                        //now that we have the correctly contracted M matrix we can construct the full D matrix:w
                        generalised_cauchy_binet<backend>::build(M1, M2, M, orb+first_orbital, occ, D);
                        temp_val += det(D, false);
                        if(count % 2 == 0)
                        {
                            R(orb, i, j) = -temp_val;
                        }
                        else
                        {
                            R(orb, i, j) = temp_val;
                        }
                    }
                }
            }
            high_resolution_clock::time_point _t2 = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
            std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
        }
        return 1;
    }

    //R2_GS = <\mu(0)\mu_GS(t1+t2)\mu_GS(t1+t2+t3)\mu(t1)\rho>
    //the ground fermi sea state resolved rephasing stimulated emission
    void R2_ground_state(matrix<complex<T>, blas_backend>& R, T ef, T tmax1, T t2, T tmax3)
    {
        T cutoff = 1e-5;
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd Green's function.  Failed to construct eigenstates.");
        }

        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt1 = tmax1/((R.shape(0)-1)*hbar);
        T dt3 = tmax3/((R.shape(1)-1)*hbar);
        t2 = t2/hbar;

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, 0.0, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<complex<T>, backend> Pkq(count, N*N);
            matrix<complex<T>, backend> Pkq_excite(count, N*N);
            matrix<complex<T>, backend> nPkq(count, N*N);
            matrix<complex<T>, backend> t1evolve(N*N, count);
            matrix<complex<T>, backend> t1evolve2(count, count);


            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);
            vector<T, blas_backend> h_ekc_excite(count);
            matrix<complex<T>, blas_backend> h_D(count, count);

            count = 0;
            for(size_t i=0; i<h_nk.size(); ++i)
            {
                if(h_nk[i] > cutoff)
                {
                    Pkq[count] = Ukqc[i];
                    nPkq[count] = h_nk[i]*Ukqc[i];
                    h_ekc[count] = h_ek[i];
                    h_D(count, count) = complex<T>(1.0 - h_nk[i], 0.0);
                    Pkq_excite[count] = Ukqc[i];
                    h_ekc_excite[count] = h_ek[i];
                    ++count;
                }
            }

            std::cout << h_ekc_excite << std::endl;
            std::cout << h_ekc << std::endl;

            matrix<complex<T>, backend> Df(h_D);
            matrix<complex<T>, backend> D(h_D);

            matrix<complex<T>, backend> temp_res(count, count);
            diagonal_matrix<T, backend> ekcm(h_ekc);
            diagonal_matrix<T, backend> ekcm_excite(h_ekc_excite);

            determinant<matrix<complex<T>, backend> > det(D, false);

            diagonal_matrix<T, backend> eqm(eq);
            diagonal_matrix<T, backend> ekm(ek);
            diagonal_matrix<complex<T>, backend> eqprop(eq.size());
            diagonal_matrix<complex<T>, backend> ekprop(ek.size());
            diagonal_matrix<complex<T>, backend> ekcprop(h_ekc.size());

            for(size_t j=0; j<R.shape(1); ++j)
            {
                std::cerr << j << std::endl;
                T t3 = j*dt3;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar(-(t2+t3)*eqm);
                t1evolve = eqprop*adjoint(Pkq);
                t1evolve2 = Pkq_excite*t1evolve;

                for(size_t i=0; i<R.shape(0); ++i)
                {
                    D = Df;
                    T t1 = i*dt1;// - tmax1;
                    ekcprop = unit_polar(-t1*ekcm);

                    D = t1evolve2*ekcprop;
                    R(i, j) = det(D, false);
                }
            }

            for(size_t i=0; i<R.shape(0); ++i)
            {
                std::cerr << i << std::endl;
                T t1 = i*dt1;

                //first we compute the temporary matrix for this time point
                eqprop = unit_polar((t1+t2)*eqm);
                t1evolve = eqprop*adjoint(Pkq_excite);
                t1evolve2 = Pkq*t1evolve;

                for(size_t j=0; j<R.shape(1); ++j)
                {
                    D = Df;
                    T t3 = j*dt3;
                    ekcprop = unit_polar(t3*ekcm_excite);

                    D = t1evolve2*ekcprop;
                    R(i, j) *= det(D, false);
                }
            }
        }
        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "R2 correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }
    
    //compute 1d current current correlation function
    void operator()(vector<complex<T>, blas_backend>& Gt, T ef, T kt, T gamma, T tmax, T cutoff = 1e-5)
    {
        T hbar = 4.135667696 * 1.0e-3 / (2.0*M_PI);//4.135668 * 1e-15 * 1e12 / (2.0*M_PI);
        if(m_requires_ham)
        {
            CALL_AND_HANDLE(construct_eigenstates(), "Failed to compute mnd_r2 Green's function.  Failed to construct eigenstates.");
        }
        high_resolution_clock::time_point _t1 = high_resolution_clock::now();

        T dt = tmax/((Gt.shape(0)-1)*hbar);

        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);

        //if there are more than one k-states that contribute to within the cutoff then we need to compute things
        if(count > 0)
        {
            matrix<T, backend> nPkq(count, N*N);            
            matrix<T, backend> tPkq(N*N, count);
            matrix<T, backend> tempr(N*N, count);
            matrix<T, backend> tempi(N*N, count);
            matrix<T, backend> eqk(N*N, count);                     

            //the result matrix.
            matrix<T, backend> Dr(count, count);
            matrix<T, backend> d_Dr(count, count);
            matrix<T, backend> Di(count, count);
            matrix<complex<T>, backend> D(count, count);

            matrix<T, blas_backend> h_Dr(count, count, 0.0);
            vector<T, blas_backend> h_eq(eq);
            vector<T, blas_backend> h_ek(ek);
            vector<T, blas_backend> h_ekc(count);

            determinant<matrix<complex<T>, backend> > det(D, false);

	        {
               	matrix<T, backend> Pkq(count, N*N);
               	count = 0;
               	for(size_t i=0; i<h_nk.size(); ++i)
               	{
               	    if(h_nk[i] > cutoff)
               	    {
               	        Pkq[count] = Ukq[i];
               	        nPkq[count] = h_nk[i]*Ukq[i];
               	        h_ekc[count] = h_ek[i];
               	        h_Dr(count, count) = (1.0 - h_nk[i]);
               	        ++count;
               	    }
               	}
               	tPkq = trans(Pkq);
	        }
	        d_Dr = h_Dr;
            
            {
                matrix<T, blas_backend> h_eqk(N*N, count, [&h_eq, &h_ekc](size_t i, size_t j){return h_eq(i) - h_ekc(j);});                     
                eqk = h_eqk;
            }

            //#pragma omp parallel for firstprivate(tempr, tempi, Dr, Di, D, det), shared(Gt, nPkq, dt, eqk, tPkq)
            for(size_t ti=0; ti < Gt.size(); ++ti)
            {
                T t = (ti*dt);//-(dt*(Gt.size()-1)/2);
                mnd_helper<T, backend>::compute(t, eqk, tPkq, tempr, tempi);
                Dr = d_Dr;
                (nPkq*tempr).applicative(Dr, 1.0);
                Di = nPkq*tempi;
                D = make_complex(Dr, Di);
                Gt(ti) = exp(complex<T>(-gamma, 0.0)*std::abs(t))*det(D, false);
            }
            
        }
        else
        {
            for(size_t ti=0; ti < Gt.size(); ++ti)
            {
                T t = ti*dt;
                Gt(ti) = exp(complex<T>(-gamma, 0.0)*t);
            }
        }


        high_resolution_clock::time_point _t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(_t2 - _t1);
        std::cerr << "correlation function computed in " << time_span.count() << " seconds" << std::endl;
    }

    void clear_temporaries()
    {
        Ukq.clear();
    }
public:
    //setters and getters for the variables required by the mnd_r2 object
    const T& electron_mass() const{ return m;}
    void set_electron_mass(const T& _m)
    {
        m = _m; 
        ASSERT(m > 0,  "Invalid argument to mnd_r2.  The electron mass must be greater than 0.");
        m_requires_ham = true;
    }

    const T& characteristic_length() const{return xi;}
    void set_characteristic_length(const T& _xi)
    {
        xi = _xi; 
        m_requires_ham = true;
    }

    const T& screening_length() const{return r0;}
    void set_screening_length(const T& _r0) 
    {
        ASSERT(_r0 > 0, "Invalid argument to mnd_r2.  The screening length must be positive.");
        r0 = _r0; 
        m_requires_ham = true;
    }

    void bind_square_grid(T a)
    {
        if(m_kgrid != nullptr){delete m_kgrid;}
        m_kgrid = new square_kgrid<T, backend>(N, a);
        m_requires_ham = true;
    }

    //void bind_hexagonal_grid(T a)
    //{
    //    if(m_kgrid != nullptr){delete m_kgrid;}
    //    m_kgrid = new hexagonal_kgrid<T, backend>(N, a);
    //    m_requires_ham = true;
    //}

    void unbind_grid()
    {
        if(m_kgrid != nullptr){delete m_kgrid;}
        m_kgrid = nullptr;
        m_requires_ham = true;
    }

public:    
    size_t noccupied(T ef, T kt, T cutoff = 1e-5)
    {
        vector<T, blas_backend> h_nk;
        size_t count = determine_state_contribution(ef, kt, h_nk, cutoff);
        return count;
    }
};

#endif  //MND_R2_HPP//

