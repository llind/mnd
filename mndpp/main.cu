/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/02/20 19:46:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lachlan Lindoy, 
 *   Organization:  Columbia University
 *
 * =====================================================================================
 */
#define CUTOFF_LENGTH
#include "mnd.hpp"
#include "mnd_r2_projection.hpp"
#include <fftw3.h>
#include <string>
#include <fstream>
#include "fft_utils.hpp"

using namespace linalg;


std::array<int, 2> wrap_frequency(int i, int N)
{
    std::array<int, 2> r;
    if(2*i < N)
    {
        r[0] = (N/2+i);
        r[1] = -(i-N/2);
    }
    else
    {
        r[0] = (i-N/2);
        r[1] = -(i-N/2);
    }
    return r;
}

template <typename T1, typename T>
void output_results_1d(const std::string& filename, const vector<complex<T>>& Gt, const vector<complex<T>>& Gw, T1 tmax, T1 delta)
{
    ASSERT(Gt.size() == Gw.size(), "Failed to output 1d spectra results.  The spectra and correlation function do not contain the same number of elements.");
    std::ofstream ofs(filename);
    T hbar = 4.135668 * 1.0e-3 / (2.0*M_PI);
    int64_t N = Gt.size();
    int64_t nt = Gt.size()-1;
    T dt = tmax/nt;
    
    vector<T> w(Gw.size());
    
    //because this is an inverse fourier transform all frequencies are inverted
    std::cerr << w.size() << std::endl;
    fft::freq(w, -static_cast<T>(tmax/(M_PI*hbar*nt)));
    fft::shift(w);

    T max = 0.0;
    for(int64_t i=0; i<Gt.size(); ++i)
    {
        if(delta + w(i) > 1e-5)
        {
            T v = real(Gw(i))/(delta+w(i));
            if(v > max)
            {
                max = v;
            }
        }

    }

    for(int64_t i=0; i<Gt.size(); ++i)
    {
        T t = dt*i;
        ofs << t << " " << real(Gt(i)) << " " << imag(Gt(i)) << " " << w(i) << " " << real(Gw(i))/((delta+w(i))*max) <<  " " << imag(Gw(i))/((delta+w(i))*max) << std::endl;
    }
    ofs.close();
}

template <typename T>
void output_dos(const std::string& filename, const vector<size_t>& dk, const vector<size_t>& dq, T wmin, T wmax, size_t N)
{
    ASSERT(dk.size() == dq.size(), "Failed to output dos the two density of state arrays were not the same size.");
    if(wmin > wmax)
    {
        T wt = wmin;
        wmin = wmax;
        wmax = wt;
    }
    T dw = (wmax - wmin)/dk.size();
    std::ofstream ofs(filename);
    for(size_t i=0; i<dk.size(); ++i)
    {
        ofs << wmin + i*dw << " " << static_cast<T>(dk(i))/(N*N) << " " << static_cast<T>(dq(i))/(N*N) << std::endl;
    }
    ofs.close();
}


template <typename T1, typename T>
void output_dos(const std::string& filename, const vector<T>& dk, const vector<T>& dq, T1 wmin, T1 wmax, size_t N)
{
    ASSERT(dk.size() == dq.size(), "Failed to output dos the two density of state arrays were not the same size.");
    if(wmin > wmax)
    {
        T1 wt = wmin;
        wmin = wmax;
        wmax = wt;
    }
    T1 dw = (wmax - wmin)/dk.size();
    std::ofstream ofs(filename);
    for(size_t i=0; i<dk.size(); ++i)
    {
        ofs << wmin + i*dw << " " << static_cast<T>(dk(i))/(N*N) << " " << static_cast<T>(dq(i))/(N*N) << std::endl;
    }
    ofs.close();
}

template <typename T1, typename Mattype>
void output_results_2d(const std::string& filename, const Mattype& Rt, T1 tmax1, T1 tmax3)
{
    std::ofstream ofsr(filename+std::string("_real"));
    std::ofstream ofsi(filename+std::string("_imag"));

    using T = typename linalg::get_real_type<typename Mattype::value_type>::type;
    T hbar = 4.135668 * 1.0e-3 / (2.0*M_PI);
    int64_t N = Rt.size();
    int64_t nt1 = Rt.shape(0)-1;
    int64_t nt3 = Rt.shape(1)-1;

    ofsr << tmax1/(2.0*M_PI*hbar) << " " << tmax3/(2.0*M_PI*hbar) << std::endl;
    ofsi << tmax1/(2.0*M_PI*hbar) << " " << tmax3/(2.0*M_PI*hbar) << std::endl;
    for(int64_t i=0; i<Rt.shape(0); ++i)
    {
        for(int64_t j=0; j<Rt.shape(1); ++j)
        {
            ofsr << real(Rt(i,j)) << " ";
        }
        ofsr << std::endl;
    }
    ofsr.close();

    for(int64_t i=0; i<Rt.shape(0); ++i)
    {
        for(int64_t j=0; j<Rt.shape(1); ++j)
        {
            ofsi << imag(Rt(i,j)) << " ";
        }
        ofsi << std::endl;
    }
    ofsi.close();
}

void run1(float ef, size_t N)
{
    using real_type = float;
#ifdef __NVCC__
    using backend = cuda_backend;
    backend::initialise(0,1);
#else
    using backend = blas_backend;
#endif

    //parameters for MoS_2
    //real_type m = 0.045;            //electron mass (eV^-1 A^-2) ~ 0.34*me_invhbar (see Phys Rev B, 99, 125421, 2019 (Yao-wen's paper))
    //real_type xi = 17.0;            //characteristic exciton length (A)
    //real_type r0 = 36.0;            //The screening length 
    //real_type delta = 2.0;


    //parameters for MoSe_2
    real_type m = 0.068;           //electron mass (eV^-1 A^-2) ~ 0.52*me_invhbar (see Durnev & Glazov, Phys. Usp., 61, 825, 2018)
    real_type xi = 16.5;           //characteristic exciton length (A)    //need to tune this to get the correct peak splitting
    real_type r0 = 51.7;           //The screening length (here choose as 2pi*chi_2d (according to Berkelbach, Reichman annu. rev. condens. matter phys. 9 379-396, 2018)  
                                  // and using the 2d polarisability for MoSe2 given in the SI of Tempelaar, Berkelbach, Nat. commun, 10, 3419, 2010)
    real_type delta = 1.66;

    real_type ec = 2.0;
    real_type ac = 2.0*M_PI/sqrt(2*m*ec);//31.9;

    //real_type ef = 0.03;           //fermi energy (eV) - fermi energy selected to get same doping density as Tempelaar and Berkelbach
    real_type kT = 0.0;             //temperature (eV)
    real_type gamma = 0.002;        //line-broadening factor (eV)


    mnd<real_type, backend> x(m, xi, r0, N);

    x.bind_square_grid(ac);
    std::cerr << x.doping_density(ef, kT) << std::endl;

    //compute density of states
    if(false)
    {
        size_t Nomega = 100000;
        real_type wmin = -0.2;
        real_type wmax =  1.0;
        vector<size_t, blas_backend> dk(Nomega);
        vector<size_t, blas_backend> dq(Nomega);

        size_t nelem = x.density_of_states(dk, dq, wmin, wmax);
        output_dos(std::string("out_dos_MoS2_")+std::to_string(N), dk, dq, wmin, wmax, nelem);
    }

    if(false)
    {
        size_t Nomega = 1000;
        real_type wmin = -0.06;
        real_type wmax =  0.02;
        vector<real_type, blas_backend> dk(Nomega);
        vector<real_type, blas_backend> dq(Nomega);

        size_t nelem = x.density_of_states(dk, dq, wmin, wmax, 0.001);
        output_dos(std::string("out_dos_MoS2_smoothed_")+std::to_string(N), dk, dq, wmin, wmax, nelem);
    }

    //compute 1d spectrum
    if(true)
    {
        real_type tmax = 10.0; 
        size_t nt = 2000;
        vector<complex<double>, blas_backend> Gt(2*nt+1);
        vector<complex<double>, blas_backend> Gw(2*nt+1);

        fftw_plan p1d;
        p1d = fftw_plan_dft_1d(Gt.size(), reinterpret_cast<fftw_complex*>(Gt.buffer()), reinterpret_cast<fftw_complex*>(Gw.buffer()), FFTW_FORWARD, FFTW_ESTIMATE);
        vector<complex<real_type>, blas_backend> _Gt(nt+1);
        x(_Gt, ef, kT, gamma, tmax);

        //now we exploit the hermiticity of this function to construct negative time values
        for(int64_t i=0; i<_Gt.size(); ++i)
        {
            //pack in the positive time values
            Gt(i) = _Gt(i);

            //pack in the negative time values
            if(i != 0){Gt(2*nt+1-i) =  conj(_Gt(i));}
        }
        fftw_execute(p1d);

        //now we shift Gw
        fft::shift(Gw);

        output_results_1d(std::string("out_1d_MoSe2_square_")+std::to_string(N)+std::string("_")+std::to_string(ef), Gt, Gw, tmax, delta);

        //matrix<complex<real_type>, blas_backend> Psi(N*10, N*10);
        //x.trion_wavefunction(Psi);
        //real_type hbar = 4.135668 * 1.0e-3 / (2.0*M_PI);
        //output_results_2d(std::string("psi_lattice_b_")+std::to_string(N), Psi, (2.0*M_PI*hbar)*N*ac, (2.0*M_PI*hbar)*N*ac);
        fftw_destroy_plan(p1d);
    }

    //compute 2d spectrum (currently only R2)
    if(true)
    {
        real_type tmax = 5.0; 
        size_t nt = 500;
        matrix<complex<real_type>, blas_backend> Rt(nt+1, nt+1);
        matrix<complex<real_type>, blas_backend> Rw(nt+1, nt+1);

        std::array<real_type, 3> tv{{0.0, 0.07, 0.14}};
        for(size_t eps_count=0; eps_count < 1; ++eps_count)
        {
            for(size_t i=0; i<=60; ++i)  
            {
                //the delay time t2
                double t2 = i*0.0025;
                //x.R1(Rt, ef, kT, tmax, i*0.05, tmax);
                //output_results_2d(std::string("out_2d_") + std::to_string(ef) + std::string("_R1_")+std::to_string(i), Rt, tmax, tmax);
                x.R2(Rt, ef, kT, tmax, t2, tmax);
                output_results_2d(std::string("out_MoSe2_")+std::to_string(N)+std::string("_2d_large_") + std::to_string(ef) + std::string("_R2_")+std::to_string(i), Rt, tmax, tmax);
                //x.R3(Rt, ef, kT, tmax, i*0.05, tmax);
                //output_results_2d(std::string("out_2d_") + std::to_string(ef) + std::string("_R3_")+std::to_string(i), Rt, tmax, tmax);
                //x.R4(Rt, ef, kT, tmax, i*0.05, tmax);
                //output_results_2d(std::string("out_2d_") + std::to_string(ef) + std::string("_R4_")+std::to_string(i), Rt, tmax, tmax);
                //x.R2_proj(Rt, ef, kT, tmax, i*0.05, tmax);
                //output_results_2d(std::string("out_2d_") + std::to_string(ef) + std::string("_projection_ground_state_R2_")+std::to_string(i), Rt, tmax, tmax);
            }
        }
    }

    //evaluate the R2 correlation function projected onto the ground state
    if(false)
    {
        real_type tmax = 5.0; 
        size_t nt = 500;
        matrix<complex<real_type>, blas_backend> Rt(nt+1, nt+1);
        matrix<complex<real_type>, blas_backend> Rw(nt+1, nt+1);
        size_t noccupied = x.noccupied(ef, kT);
        x.R2_proj(Rt, ef, kT, tmax, 0.0, tmax, noccupied+10);
        output_results_2d(std::string("out_")+std::to_string(N)+std::string("_2d_") + std::to_string(ef) + std::string("_projection_ground_state_R2_")+std::to_string(0), Rt, tmax, tmax);
    }


    fftw_cleanup();
#ifdef __NVCC__
    backend::destroy();
#endif
}

void run2(float ef, size_t N)
{
    using real_type = float;
#ifdef __NVCC__
    using backend = cuda_backend;
    backend::initialise(0,1);
#else
    using backend = blas_backend;
#endif

    //parameters for MoS_2
    //real_type m = 0.045;            //electron mass (eV^-1 A^-2) ~ 0.34*me_invhbar (see Phys Rev B, 99, 125421, 2019 (Yao-wen's paper))
    //real_type xi = 17.0;            //characteristic exciton length (A)
    //real_type r0 = 36.0;            //The screening length 
    //real_type delta = 2.0;



    //parameters for MoSe_2
    real_type m = 0.068;           //electron mass (eV^-1 A^-2) ~ 0.52*me_invhbar (see Durnev & Glazov, Phys. Usp., 61, 825, 2018)
    real_type xi = 16.5;           //characteristic exciton length (A)    //need to tune this to get the correct peak splitting
    real_type r0 = 51.7;           //The screening length (here choose as 2pi*chi_2d (according to Berkelbach, Reichman annu. rev. condens. matter phys. 9 379-396, 2018)  
                                  // and using the 2d polarisability for MoSe2 given in the SI of Tempelaar, Berkelbach, Nat. commun, 10, 3419, 2010)
    real_type delta = 1.66;

    real_type ec = 2.0;
    real_type ac = 2.0*M_PI/sqrt(2*m*ec);//31.9;

    //real_type ef = 0.01;           //fermi energy (eV) - fermi energy selected to get same doping density as Tempelaar and Berkelbach
    real_type kT = 0.0;             //temperature (eV)
    real_type gamma = 0.002;        //line-broadening factor (eV)

    mnd_r2<real_type, backend> x(m, xi, r0, N);

    x.bind_square_grid(ac);
    std::cerr << x.doping_density(ef, kT) << std::endl;
    if(false)
    {
        size_t Nomega = 1000;
        real_type wmin = -0.06;
        real_type wmax =  0.02;
        vector<size_t, blas_backend> dk(Nomega);
        vector<size_t, blas_backend> dq(Nomega);

        size_t nelem = x.density_of_states(dk, dq, wmin, wmax);
        output_dos(std::string("out_dos_MoS2_")+std::to_string(N), dk, dq, wmin, wmax, nelem);
    }

    //compute 1d spectrum
    if(false)
    {
        real_type tmax = 10.0; 
        size_t nt = 2000;
        vector<complex<double>, blas_backend> Gt(2*nt+1);
        vector<complex<double>, blas_backend> Gw(2*nt+1);

        fftw_plan p1d;
        p1d = fftw_plan_dft_1d(Gt.size(), reinterpret_cast<fftw_complex*>(Gt.buffer()), reinterpret_cast<fftw_complex*>(Gw.buffer()), FFTW_FORWARD, FFTW_ESTIMATE);
        vector<complex<real_type>, blas_backend> _Gt(nt+1);
        x(_Gt, ef, kT, gamma, tmax);

        //now we exploit the hermiticity of this function to construct negative time values
        for(int64_t i=0; i<_Gt.size(); ++i)
        {
            //pack in the positive time values
            Gt(i) = _Gt(i);

            //pack in the negative time values
            if(i != 0){Gt(2*nt+1-i) =  conj(_Gt(i));}
        }
        fftw_execute(p1d);

        //now we shift Gw
        fft::shift(Gw);

        output_results_1d(std::string("out_1d_MoS2_square_")+std::to_string(N), Gt, Gw, tmax, delta);
        fftw_destroy_plan(p1d);
    }
    //evaluate the ground state projected spectrum
    if(true)
    {
        real_type tmax = 5.0; 
        size_t nt = 500;
        matrix<complex<real_type>, blas_backend> Rt(nt+1, nt+1);
        matrix<complex<real_type>, blas_backend> Rw(nt+1, nt+1);
        size_t noccupied = x.noccupied(ef, kT);
        for(size_t i=0; i<=20; ++i)  
        {
            double t2 = i*0.03;
            x.R2_ground_state(Rt, ef, tmax, t2, tmax);
            output_results_2d(std::string("out_MoSe2_")+std::to_string(N)+std::string("_2d_") + std::to_string(ef) + std::string("_") + std::to_string(ec) + std::string("_projection_ground_state_R2_")+std::to_string(i), Rt, tmax, tmax);
        }
    }

    if(false)
    {
        real_type tmax = 5.0; 
        size_t nt = 500;
        matrix<complex<real_type>, blas_backend> Rt(nt+1, nt+1);
        matrix<complex<real_type>, blas_backend> Rw(nt+1, nt+1);
        size_t noccupied = x.noccupied(ef, kT);
        std::cerr << noccupied << std::endl;
        x.R2_single_excitation(Rt, ef, tmax, 0.0, tmax);
        output_results_2d(std::string("out_")+std::to_string(N)+std::string("_2d_") + std::to_string(ef) + std::string("_projection_singly_excited_state_R2_gpu_")+std::to_string(0), Rt, tmax, tmax);
        std::cerr << noccupied << std::endl;
    }

    //evaluate correlation function obtained when the final state is a single excitation away from the ground state. 
    if(false)
    {
        real_type tmax = 5.0; 
        size_t nt = 500;
        size_t noccupied = x.noccupied(ef, kT);
        size_t nconcurrent = noccupied;
        tensor<complex<real_type>, 3, blas_backend> Rt(nconcurrent, nt+1, nt+1);
        matrix<complex<real_type>, blas_backend> Rttot(nt+1, nt+1);
        std::cerr << noccupied << std::endl;
        for(size_t i=0; i<noccupied; i+=nconcurrent)
        {
            x.R2_single_excitation(Rt, ef, tmax, 0.0, tmax, i);

            for(size_t j=0; j < nconcurrent && j+i < noccupied; ++j)
            {
                output_results_2d(std::string("out_")+std::to_string(N)+std::string("_2d_") + std::to_string(ef) + std::string("_projection_singly_excited_") + std::to_string(j+i) + std::string("_R2_gpu_")+std::to_string(0), Rt[j], tmax, tmax);
                Rttot += Rt[j];
            }
        }
        output_results_2d(std::string("out_")+std::to_string(N)+std::string("_2d_") + std::to_string(ef) + std::string("_projection_singly_excited_states") + std::string("_R2_gpu_")+std::to_string(0), Rttot, tmax, tmax);
        std::cerr << noccupied << std::endl;
    }

    fftw_cleanup();
#ifdef __NVCC__
    backend::destroy();
#endif
}


int main(int argc, char* argv[])
{
    //run1(0.005, 120);
    //run2(0.005, 120);
    run1(0.01, 120);
    //run2(0.01, 120);
    //run1(0.02, 120);
    //run2(0.02, 120);
}

