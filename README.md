# MNDPP

Evaluation of 1D and 2D spectra for 2D systems modelled using an exciton-electron scattering form of a Mahan-Nozieres-De Dominicis Hamiltonian.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This library depends upon LAPACK and BLAS libraries.  We now use the cmake FindBlas routine in order to find vendor specific versions of BLAS.

This library makes use of the cereal serialisation library for saving checkpoint files.  If this library is not found, checkpoint will not be available.

### Installing

This program uses cmake as a build tool.  From the top directory this software can be installed using

mkdir build

cd build

cmake ../

make

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Lachlan Lindoy** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

