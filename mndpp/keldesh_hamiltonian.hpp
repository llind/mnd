#ifndef KELDESH_HAMILTONIAN_HPP
#define KELDESH_HAMILTONIAN_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <linalg/dense.hpp>

using namespace linalg;

template <typename T, typename backend>
class keldesh_hamiltonian;

template <typename T>
class keldesh_hamiltonian<T, blas_backend>
{
public:
    static void construct(matrix<T, blas_backend>& H, const vector<T, blas_backend>& _kx, const vector<T, blas_backend>& _ky, const T& _V, const T& _m, const T& _xi, const T& _r0)
    {
        H.fill(
            [](size_t i, size_t j, const T* const kx, const T* const ky, T V, T m, T xi, T r0, T e2)
            {
                T kix = kx[i];   T kiy = ky[i];
                T kjx = kx[j];   T kjy = ky[j];

                T kij = sqrt((kix - kjx)*(kix - kjx) + (kiy - kjy)*(kiy - kjy));

                T Hij = 0.0;
                if(fabs(kij) > 1e-12)
                {
                    T v = -(2.0*M_PI*e2)/(kij*(1.0+r0*kij));
                    Hij = v/V*(1.0 - exp(-0.5*kij*kij*xi*xi));
                }
                return Hij + (i == j ? (kix*kix + kiy*kiy)/(2.0*m) : 0.0);
            },
            _kx.buffer(), _ky.buffer(), _V, _m, _xi, _r0, 14.3996
        );
    }
};

#ifdef __NVCC__

template <typename T>
class keldesh_hamiltonian<T, cuda_backend>
{
public:
    static void construct(matrix<T, cuda_backend>& H, const vector<T, cuda_backend>& _kx, const vector<T, cuda_backend>& _ky, const T& _V, const T& _m, const T& _xi, const T& _r0)
    {
        H.fill(
            [] __host__ __device__ (size_t i, size_t j, const T* const kx, const T* const ky, T V, T m, T xi, T r0, T e2)
            {
                T kix = kx[i];   T kiy = ky[i];
                T kjx = kx[j];   T kjy = ky[j];

                T kij = sqrt((kix - kjx)*(kix - kjx) + (kiy - kjy)*(kiy - kjy));

                T Hij = 0.0;
                if(fabs(kij) > 1e-12)
                {
                    T v = -(2.0*M_PI*e2)/(kij*(1.0+r0*kij));
                    Hij = v/V*(1.0 - exp(-0.5*kij*kij*xi*xi));
                }
                return Hij + (i == j ? (kix*kix + kiy*kiy)/(2.0*m) : 0.0);
            },
            _kx.buffer(), _ky.buffer(), _V, _m, _xi, _r0, 14.3996
        );
    }    
};

#endif


#endif  //KELDESH_HAMILTONIAN_HPP


