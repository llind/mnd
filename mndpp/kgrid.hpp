#ifndef KGRID_HPP
#define KGRID_HPP

#include <linalg/dense.hpp>

using namespace linalg;

//base class for 2d NxN k-point grids
template <typename T, typename backend>
class kgrid
{
protected:
    vector<T, backend> _kx;
    vector<T, backend> _ky;
    size_t _N;
    bool m_constructed;

public:
    kgrid() : _N(0), m_constructed(false) {}

    kgrid(size_t N) try : _kx(N*N), _ky(N*N), _N(N), m_constructed(false) {}
    catch(const std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        RAISE_EXCEPTION("Failed to construct kgrid.");
    }

    virtual ~kgrid(){};
    virtual void construct()
    {
        ASSERT(_N != 0, "Failed to construct 2d k-grid.  Cannot construct a 0x0 grid.");
        m_constructed = true;
    }

    void resize(size_t N)
    {
        CALL_AND_HANDLE(_kx.resize(N), "Failed to resize kgrid.  Failed to resize kx.");
        CALL_AND_HANDLE(_ky.resize(N), "Failed to resize kgrid.  Failed to resize ky.");
        m_constructed = false;
    }

    size_t N() const {return _N;}

    const vector<T, backend>& kx() const {return _kx;}
    const vector<T, backend>& ky() const {return _ky;}

    bool lattice_constructed(){return m_constructed;}
    virtual T V() = 0;
};




//NxN 2d square k-point grid
template <typename T, typename backend>
class square_kgrid;

//blas implementation
template <typename T>
class square_kgrid<T, blas_backend> : public kgrid<T, blas_backend>
{
    using base_type = kgrid<T, blas_backend>;
protected:
    T _a;        //The lattice parameter in A

public:
    square_kgrid() : base_type(), _a(0)  {}
    square_kgrid(size_t N) try : base_type(N), _a(0) {}
    catch(const std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        RAISE_EXCEPTION("Failed to construct square_kgrid.");
    }

    square_kgrid(size_t N, T a) try : base_type(N), _a(a) {}
    catch(const std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        RAISE_EXCEPTION("Failed to construct square_kgrid.");
    }

    ~square_kgrid(){}

    void construct()
    {
        ASSERT(_a > T(0), "Failed to construct square lattice k-grid.  The lattice parameter is invalid.");
        CALL_AND_RETHROW(base_type::construct());

        T L = base_type::_N*_a;
        base_type::_kx.fill([](size_t i, size_t N, T _L){return (i/N)*2.0*M_PI/_L;},base_type::_N, L );
        base_type::_ky.fill([](size_t i, size_t N, T _L){return (i%N)*2.0*M_PI/_L;},base_type::_N, L );
    }

    T V(){return (base_type::_N*_a)*(base_type::_N*_a);}
};


#ifdef __NVCC__
template <typename T>
class square_kgrid<T, cuda_backend> : public kgrid<T, cuda_backend>
{
    using base_type = kgrid<T, cuda_backend>;
protected:
    T _a;        //The lattice parameter in A

public:
    square_kgrid() : base_type(), _a(0)  {}
    square_kgrid(size_t N) try : base_type(N), _a(0) {}
    catch(const std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        RAISE_EXCEPTION("Failed to construct square_kgrid.");
    }

    square_kgrid(size_t N, T a) try : base_type(N), _a(a) {}
    catch(const std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        RAISE_EXCEPTION("Failed to construct square_kgrid.");
    }

    ~square_kgrid(){}

    void construct()
    {
        ASSERT(_a > T(0), "Failed to construct square lattice k-grid.  The lattice parameter is invalid.");
        CALL_AND_RETHROW(base_type::construct());

        T L = base_type::_N*_a;
        base_type::_kx.fill([] __host__ __device__ (size_t i, size_t N, T L){return (i/N)*2.0*M_PI/L;},base_type::_N, L );
        base_type::_ky.fill([] __host__ __device__ (size_t i, size_t N, T L){return (i%N)*2.0*M_PI/L;},base_type::_N, L );
    }
    
    T V(){return (base_type::_N*_a)*(base_type::_N*_a);}
};
#endif

#endif

