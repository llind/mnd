#ifndef FFT_UTILS_HPP
#define FFT_UTILS_HPP

#include <linalg/dense.hpp>
#include <cstdint>

using namespace linalg;

namespace fft
{

template <typename T> 
void freq(linalg::vector<T> & w, typename get_real_type<T>::type t)
{
    int64_t N = w.size();
    if(N & 1)
    {
        int64_t midpoint = (N-1)/2;
        for(int64_t i=0; i<midpoint+1; ++i){w(i) = i/(t*N);}
        for(int64_t i=midpoint+1; i<N; ++i){w(i) = (i-static_cast<int64_t>(N))/(t*N);}
    }
    else
    {
        int64_t midpoint = N/2;
        for(int64_t i=0; i<midpoint; ++i){w(i) = i/(t*N);}
        for(int64_t i=midpoint; i<N; ++i){w(i) = (i-static_cast<int64_t>(N))/(t*N);}
    }
}

template <typename T> 
void shift(linalg::vector<T>& x)
{
    int64_t N = x.size();
    if(N & 1)
    {
        int64_t midpoint = (N-1)/2;
        
        //first we go ahead and swap the 
        T mid = x(midpoint);
        for(int64_t i=0; i<midpoint; ++i)
        {
            x(i + midpoint) = x(i);
            x(i) = x(i + midpoint + 1);
        }
        x(N-1) = mid;
    }
    else
    {
        int64_t midpoint = N/2;
        for(int64_t i=0; i<midpoint; ++i)
        {
            T temp = x(i+midpoint);
            x(i + midpoint) = x(i);
            x(i) = temp;
        }
    }
}

template <typename T> 
void ishift(linalg::vector<T>& x)
{
    int64_t N = x.size();
    if(N & 1)
    {
        int64_t midpoint = (N-1)/2;
        
        //first we go ahead and swap the 
        T mid = x(midpoint);
        for(int64_t i=0; i<midpoint; ++i)
        {
            x(midpoint-i) = x(N-i-1);
            x(N-i-1) = x(midpoint - i - 1);
        }
        x(0) = mid;
    }
    //for an even length array ishift is just the same as shift
    else
    {
        int64_t midpoint = N/2;
        for(int64_t i=0; i<midpoint; ++i)
        {
            T temp = x(i+midpoint);
            x(i + midpoint) = x(i);
            x(i) = temp;
        }
    }
}

}

#endif //FFT_UTILS_HPP

